package com.reciplay.cis.beans;

public class IntermStatsData {

	private long userId;
	private String runningStatsData;
	private String previousStatsData;
	private long lastStatsViewTime;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getRunningStatsData() {
		return runningStatsData;
	}
	public void setRunningStatsData(String runningStatsData) {
		this.runningStatsData = runningStatsData;
	}
	public String getPreviousStatsData() {
		return previousStatsData;
	}
	public void setPreviousStatsData(String previousStatsData) {
		this.previousStatsData = previousStatsData;
	}
	public long getLastStatsViewTime() {
		return lastStatsViewTime;
	}
	public void setLastStatsViewTime(long lastStatsViewTime) {
		this.lastStatsViewTime = lastStatsViewTime;
	}
	public IntermStatsData(long userId, String runningStatsData, String previousStatsData, long lastStatsViewTime) {
		this.userId = userId;
		this.runningStatsData = runningStatsData;
		this.previousStatsData = previousStatsData;
		this.lastStatsViewTime = lastStatsViewTime;
	}
	public IntermStatsData() {
	}
	
	
}
