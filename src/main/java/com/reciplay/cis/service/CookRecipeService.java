package com.reciplay.cis.service;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.reciplay.cis.beans.PayloadInfo;
import com.reciplay.cis.common.CommonServiceHelper;
import com.reciplay.cis.dao.RecipeActionDAOService;

@Service
public class CookRecipeService {
private static final Logger logger = LoggerFactory.getLogger(CookRecipeService.class);
	
	@Autowired
	RecipeActionDAOService recipeActionDAOService;
	

	@Autowired
	CommonServiceHelper serviceHelper;
	
	@Transactional("transactionManagerMaster")
	public void cookAction(PayloadInfo payloadInfo) {
		try {
			logger.info("Starting action publishAction.");
			// Insert in action_publish table
			recipeActionDAOService.insertActionCookTable(payloadInfo.getUserId(), payloadInfo.getRecipeId());
			// Update recipe_stats table and update the browse count
			if(serviceHelper.checkIfCurrentMonthEntryIsThere(payloadInfo.getRecipeId())) {
				// Means we have to increment browsed_count total = total +1 and browsed_current_month = current +1
				recipeActionDAOService.updateRecipeStatsTableCook(payloadInfo.getRecipeId());
			} else {
				// Fresh data for current month. Get previous month data.
				LocalDate currentDate = LocalDate.now(); // 2016-06-17 
				int month = currentDate.getMonthValue(); // 17 
				int year = currentDate.getYear(); // 169
				//Update previous month data , increment total and this month data in recipe stats table.
				int countPreviousMonth = recipeActionDAOService.cookCountForMonth(payloadInfo.getRecipeId(),month-1,year);
				recipeActionDAOService.updateRecipeStatsTableCookPreviousMonthData(payloadInfo.getRecipeId(),countPreviousMonth);
			}
			//Update user_interim_stats table
			serviceHelper.updateIntermStatsDataForTheUser(payloadInfo);

			 
		} catch (Exception e) {
			System.out.println("Err == " + e.getCause());
			logger.error("Error browseAction ", e.getMessage());
			throw e;
		}

	}
}
