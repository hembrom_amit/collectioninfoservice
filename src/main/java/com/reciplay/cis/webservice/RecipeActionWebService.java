package com.reciplay.cis.webservice;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reciplay.cis.beans.PayloadInfo;

import com.reciplay.cis.common.Constants.WebConstant;
import com.reciplay.cis.common.ErrorHelper;
import com.reciplay.cis.common.ServiceException;
import com.reciplay.cis.loggicaspect.LogginAspect;
import com.reciplay.cis.service.AuthorizationService;
import com.reciplay.cis.service.BrowseRecipeService;
import com.reciplay.cis.service.CookRecipeService;
import com.reciplay.cis.service.PublishRecipeService;
import com.reciplay.cis.service.RecipeActionService;
import com.reciplay.cis.common.Constants;

@Service
@Path("/recipeaction")
public class RecipeActionWebService {

	
	private static final Logger logger = LoggerFactory.getLogger(RecipeListWebService.class);

	@Autowired
	ErrorHelper errHelper;

	@Autowired
	RecipeActionService recipeActionService;

	@Autowired
	PublishRecipeService publishRecipeService;
	
	@Autowired
	BrowseRecipeService browseRecipeService;
	
	@Autowired
	CookRecipeService cookRecipeService;
	
	@Autowired
	AuthorizationService authorizationService;
	
	@POST
	@Path("/browse/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@LogginAspect
	public Response recipeBrowse(@Context ContainerRequestContext crc,String payload) throws ServiceException {

		try {
			// check whether payload is null or empty
			if (payload == null || payload.isEmpty()) {
				throw new ServiceException(Status.BAD_REQUEST, WebConstant.PAYLOAD_NULL_OR_EMPTY,
						WebConstant.PAYLOAD_NULL_OR_EMPTY_ERRCODE);
			}
			logger.info("Input payload recipeBrowse "+payload);
			PayloadInfo browseInfo = recipeActionService.convertPayloadToBrowseInfo(new JSONObject(payload));
			
			JSONObject jsonObject = (JSONObject) crc.getProperty(Constants.WebConstant.AUTHENTICATION_DATA);
			long userIdFromToken = jsonObject.getLong(Constants.WebConstant.USERID);

			if (userIdFromToken != browseInfo.getUserId()) {
				throw new ServiceException(Status.UNAUTHORIZED, WebConstant.INVALID_USERID,
						WebConstant.INVALID_USERID_ERRCODE);
			}

			authorizationService.validateRecipeInfoForUser(browseInfo.getUserId(), browseInfo.getRecipeId());
			
			browseRecipeService.browseAction(browseInfo);
			return Response.status(Response.Status.OK).build();
		} catch (JSONException e) {
			return Response.status(Status.BAD_REQUEST)
					.entity(errHelper.makeErrorCode(WebConstant.INVALID_JSON, WebConstant.INVALID_JSON_ERRCODE))
					.build();
		} catch (ServiceException se) {
			return Response.status(se.getStatus())
					.entity(errHelper.makeErrorCodeMap(se.getError(), se.getBusinessErrorCode(), se.getKeyValuemap()))
					.build();
		} catch (Exception ex) {
			logger.error("Error :: "+ex);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(errHelper.makeErrorCode(WebConstant.UNABLE_TO_PROCESS_REQUEST,
							WebConstant.UNABLE_TO_PROCESS_REQUEST_ERRCODE)  )
					.build();
		}
	}
	
	@POST
	@Path("/cook/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@LogginAspect
	public Response cookRecipe(@Context ContainerRequestContext crc,String payload) throws ServiceException {

		try {
			// check whether payload is null or empty
			if (payload == null || payload.isEmpty()) {
				throw new ServiceException(Status.BAD_REQUEST, WebConstant.PAYLOAD_NULL_OR_EMPTY,
						WebConstant.PAYLOAD_NULL_OR_EMPTY_ERRCODE);
			}
			logger.info("Input payload cookRecipe "+payload);
			PayloadInfo cookInfo = recipeActionService.convertPayloadToBrowseInfo(new JSONObject(payload));
			
			JSONObject jsonObject = (JSONObject) crc.getProperty(Constants.WebConstant.AUTHENTICATION_DATA);
			long userIdFromToken = jsonObject.getLong(Constants.WebConstant.USERID);

			if (userIdFromToken != cookInfo.getUserId()) {
				throw new ServiceException(Status.UNAUTHORIZED, WebConstant.INVALID_USERID,
						WebConstant.INVALID_USERID_ERRCODE);
			}
			
			authorizationService.validateRecipeInfoForUser(cookInfo.getUserId(), cookInfo.getRecipeId());
			cookRecipeService.cookAction(cookInfo);
			return Response.status(Response.Status.OK).build();
		} catch (JSONException e) {
			return Response.status(Status.BAD_REQUEST)
					.entity(errHelper.makeErrorCode(WebConstant.INVALID_JSON, WebConstant.INVALID_JSON_ERRCODE))
					.build();
		} catch (ServiceException se) {
			return Response.status(se.getStatus())
					.entity(errHelper.makeErrorCodeMap(se.getError(), se.getBusinessErrorCode(), se.getKeyValuemap()))
					.build();
		} catch (Exception ex) {
			logger.error("Error :: "+ex);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(errHelper.makeErrorCode(WebConstant.UNABLE_TO_PROCESS_REQUEST,
							WebConstant.UNABLE_TO_PROCESS_REQUEST_ERRCODE)  )
					.build();
		}
	}
	
	
	@POST
	@Path("/publish/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@LogginAspect
	public Response recipePublish(@Context ContainerRequestContext crc,String payload) throws ServiceException {

		try {
			// check whether payload is null or empty
			if (payload == null || payload.isEmpty()) {
				throw new ServiceException(Status.BAD_REQUEST, WebConstant.PAYLOAD_NULL_OR_EMPTY,
						WebConstant.PAYLOAD_NULL_OR_EMPTY_ERRCODE);
			}
			logger.info("Input payload recipePublish "+payload);
			PayloadInfo payloadInfo= recipeActionService.convertPayloadToBrowseInfo(new JSONObject(payload));
			
			JSONObject jsonObject = (JSONObject) crc.getProperty(Constants.WebConstant.AUTHENTICATION_DATA);
			long userIdFromToken = jsonObject.getLong(Constants.WebConstant.USERID);

			if (userIdFromToken != payloadInfo.getUserId()) {
				throw new ServiceException(Status.UNAUTHORIZED, WebConstant.INVALID_USERID,
						WebConstant.INVALID_USERID_ERRCODE);
			}
			
			authorizationService.validateRecipeInfoForUser(payloadInfo.getUserId(), payloadInfo.getRecipeId());
			
			publishRecipeService.publishAction(payloadInfo);
			return Response.status(Response.Status.OK).build();
		} catch (JSONException e) {
			return Response.status(Status.BAD_REQUEST)
					.entity(errHelper.makeErrorCode(WebConstant.INVALID_JSON, WebConstant.INVALID_JSON_ERRCODE))
					.build();
		} catch (ServiceException se) {
			return Response.status(se.getStatus())
					.entity(errHelper.makeErrorCodeMap(se.getError(), se.getBusinessErrorCode(), se.getKeyValuemap()))
					.build();
		} catch (Exception ex) {
			logger.error("Error :: "+ex);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(errHelper.makeErrorCode(WebConstant.UNABLE_TO_PROCESS_REQUEST,
							WebConstant.UNABLE_TO_PROCESS_REQUEST_ERRCODE)  )
					.build();
		}
	}
	
	
	@GET
	@Path("/getpublished/{userId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getpublished(@Context ContainerRequestContext crc,@PathParam("userId") long userId,@QueryParam("sortBy") int sortBy,@QueryParam("newlist") String newlist,
			@QueryParam("page") int page,@QueryParam("recipePerPage") int recipePerPage ) throws ServiceException {

		try {
			if (userId<=0) {
				throw new ServiceException(Status.BAD_REQUEST, WebConstant.INVALID_USERID,
						WebConstant.INVALID_USERID_ERRCODE);
			}
			if (page<0) {
				throw new ServiceException(Status.BAD_REQUEST, WebConstant.INVALID_PAGEID,
						WebConstant.INVALID_PAGEID_ERRCODE);
			}
			if (sortBy<0 || sortBy>5) {
				throw new ServiceException(Status.BAD_REQUEST, WebConstant.INVALID_SORTBY,
						WebConstant.INVALID_SORTBY_ERRCODE);
			}
			if (recipePerPage<0  ) {
				throw new ServiceException(Status.BAD_REQUEST, WebConstant.INVALID_RECEIPEPERPAGE,
						WebConstant.INVALID_RECEIPEPERPAGE_ERRCODE);
			}
			
			JSONObject jsonObject = (JSONObject) crc.getProperty(Constants.WebConstant.AUTHENTICATION_DATA);
			long userIdFromToken = jsonObject.getLong(Constants.WebConstant.USERID);

			if (userIdFromToken != userId) {
				throw new ServiceException(Status.UNAUTHORIZED, WebConstant.INVALID_USERID,
						WebConstant.INVALID_USERID_ERRCODE);
			}

			
			
			JSONObject info = recipeActionService.getPublishedRecipe(userId,sortBy,newlist,page,recipePerPage);
			return Response.status(Status.OK).entity(info.toString()).build();
		} catch (JSONException e) {
			return Response.status(Status.BAD_REQUEST)
					.entity(errHelper.makeErrorCode(WebConstant.INVALID_JSON, WebConstant.INVALID_JSON_ERRCODE))
					.build();
		} catch (ServiceException se) {
			return Response.status(se.getStatus())
					.entity(errHelper.makeErrorCodeMap(se.getError(), se.getBusinessErrorCode(), se.getKeyValuemap()))
					.build();
		} catch (Exception ex) {
			logger.error("Error :: "+ex);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(errHelper.makeErrorCode(WebConstant.UNABLE_TO_PROCESS_REQUEST,
							WebConstant.UNABLE_TO_PROCESS_REQUEST_ERRCODE)  )
					.build();
		}
	}
	
	
}
