package com.reciplay.cis.common;

import java.time.LocalDate;
import java.util.List;

import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.reciplay.cis.beans.IntermStatsData;
import com.reciplay.cis.beans.PayloadInfo;
import com.reciplay.cis.beans.UserRecipeStatsInfo;
import com.reciplay.cis.common.Constants.WebConstant;
import com.reciplay.cis.dao.RecipeActionDAOService;
import com.reciplay.cis.webservice.RecipeListWebService;

@Service
public class CommonServiceHelper {

	private static final Logger logger = LoggerFactory.getLogger(CommonServiceHelper.class);

	@Autowired
	RecipeActionDAOService recipeActionDAOService;
	

	@Autowired
	CISUtility util;
	
	public void updateIntermStatsDataForTheUser(PayloadInfo browseInfo) {
		// Taking lock.
		long chefId = recipeActionDAOService.getChefIdForRecipe(browseInfo.getRecipeId());
		UserRecipeStatsInfo internStatsData = getUserPublishedRecipeStatsInfo(chefId);
		if (internStatsData != null) {
			String currDataUpdated = convertRecipeStatsInfoToString(internStatsData);
			recipeActionDAOService.updateIntermStatsData(chefId, currDataUpdated);
		}  
	}
	
	private UserRecipeStatsInfo getUserPublishedRecipeStatsInfo(long chefId) {
		UserRecipeStatsInfo userStats =null;
		List<Long> recipeIds = recipeActionDAOService.getAllPublishedRecipeIds(chefId);
		if(recipeIds!=null && !recipeIds.isEmpty()) {

			String allRecipeStr = util.getRecipeString(recipeIds);
			userStats = recipeActionDAOService.getStatsDataForUser(allRecipeStr);
		}
		return userStats;
	}
	
	public IntermStatsData prepareFirstTimeIntermData(long userId) {
		UserRecipeStatsInfo userData = new UserRecipeStatsInfo();
		userData.setBrowseCount(1);
		IntermStatsData intermData =  new IntermStatsData();
		intermData.setPreviousStatsData(null);
		intermData.setRunningStatsData(convertRecipeStatsInfoToString(userData));
		return intermData;
	}

	


	public String convertRecipeStatsInfoToString(UserRecipeStatsInfo recipeStatsInfo) {
		String str = "";
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		try {
			str = mapper.writeValueAsString(recipeStatsInfo);
		} catch (Exception ex) {
			throw new ServiceException(Status.BAD_REQUEST, WebConstant.IMPROPER_PAYLOAD,
					WebConstant.IMPROPER_PAYLOAD_ERRCODE, ex);
		}

		logger.info("RecipeStatsInfo POJO String " + str);
		return str;
	}
	
	public boolean checkIfCurrentMonthEntryIsThere(long recipeId) {
		LocalDate currentDate = LocalDate.now(); // 2016-06-17 
		int month = currentDate.getMonthValue(); // 17 
		int year = currentDate.getYear(); // 169

		if(recipeActionDAOService.checkRecipeHistory(month,year,recipeId)) {
			return true;
		}
		recipeActionDAOService.insertIntoRecipeHistory(month,year,recipeId);
		return false;
		
	}

	

	
	

	
}
