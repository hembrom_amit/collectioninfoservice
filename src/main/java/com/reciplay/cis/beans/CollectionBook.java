package com.reciplay.cis.beans;

public class CollectionBook {

	private long userId;
	private long recipeId;
	private String recipeName;
	private String authorName;
	private String dishStatus;
	private long lastVisitTime;
	
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getRecipeId() {
		return recipeId;
	}
	public void setRecipeId(long recipeId) {
		this.recipeId = recipeId;
	}
	public String getRecipeName() {
		return recipeName;
	}
	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public String getDishStatus() {
		return dishStatus;
	}
	public void setDishStatus(String dishStatus) {
		this.dishStatus = dishStatus;
	}
	public long getLastVisitTime() {
		return lastVisitTime;
	}
	public void setLastVisitTime(long lastVisitTime) {
		this.lastVisitTime = lastVisitTime;
	}
	
	public CollectionBook() {
	}
	public CollectionBook(long recipeId, String recipeName, String authorName, String dishStatus) {
		this.recipeId = recipeId;
		this.recipeName = recipeName;
		this.authorName = authorName;
		this.dishStatus = dishStatus;
	}
	@Override
	public String toString() {
		return "CollectionBook [userId=" + userId + ", recipeId=" + recipeId + ", recipeName=" + recipeName
				+ ", authorName=" + authorName + ", dishStatus=" + dishStatus + ", lastVisitTime=" + lastVisitTime
				+ "]";
	}
	
	
}
