package com.reciplay.cis.service;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reciplay.cis.beans.PayloadInfo;
import com.reciplay.cis.common.CommonServiceHelper;
import com.reciplay.cis.dao.RecipeActionDAOService;

@Service
public class BrowseRecipeService {

private static final Logger logger = LoggerFactory.getLogger(BrowseRecipeService.class);
	
	@Autowired
	RecipeActionDAOService recipeActionDAOService;
	
	@Autowired
	CommonServiceHelper serviceHelper;
	
	public void browseAction(PayloadInfo browseInfo) {
		try {
			logger.info("Starting action browseAction.");

			// Insert or update +1 count recipe_stats table and update the browse count

			// Check if the recipe is published
			//Boolean ifPublishedRecipe = recipeActionDAOService.ifPublishedRecipe(browseInfo.getRecipeId());

			// Insert in action_browse table
			recipeActionDAOService.insertActionBrowseTable(browseInfo.getUserId(), browseInfo.getRecipeId());
			logger.info("insertActionBrowseTable Done.");
			
			// Update recipe_stats table and update the browse count
			if(serviceHelper.checkIfCurrentMonthEntryIsThere(browseInfo.getRecipeId())) {
				// Means we have to increment browsed_count total = total +1 and browsed_current_month = current +1
				recipeActionDAOService.updateRecipeStatsTableBrowse(browseInfo.getRecipeId());
			} else {
				// Fresh data for current month. Get previous month data.
				LocalDate currentDate = LocalDate.now(); // 2016-06-17 
				int month = currentDate.getMonthValue(); // 17 
				int year = currentDate.getYear(); // 169
				int countPreviousMonth = recipeActionDAOService.browseCountForMonth(browseInfo.getRecipeId(),month-1,year);
				recipeActionDAOService.updateRecipeStatsTableBrowseCurrentMonthData(browseInfo.getRecipeId(),countPreviousMonth);
			}
			//Update user_interim_stats table
			serviceHelper.updateIntermStatsDataForTheUser(browseInfo);
		
			// Check if the recipe is favorite by the user , if yes update the same in
			// user_favorite_recipe_info table.

			if (recipeActionDAOService.checkIfFavorite(browseInfo.getUserId(), browseInfo.getRecipeId())) {
				logger.info("checkIfFavorite  true");
				long browseId = recipeActionDAOService.getUserBrowseId(browseInfo.getUserId(),
						browseInfo.getRecipeId());
				if (browseId > 0) {
					logger.info("browseId " + browseId);
					recipeActionDAOService.updateUserFavoriteRecipeInfo(browseInfo.getUserId(),
							browseInfo.getRecipeId(), browseId);
					logger.info("updateUserFavoriteRecipeInfo " + browseId);
				}
			}
		} catch (Exception e) {
			System.out.println("Err == " + e.getCause());
			logger.error("Error browseAction ", e.getMessage());
			throw e;
		}

	}
	

}
