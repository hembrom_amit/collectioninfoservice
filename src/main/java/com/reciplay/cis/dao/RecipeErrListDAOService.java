package com.reciplay.cis.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public final class RecipeErrListDAOService {

	@Autowired
	SqlSessionTemplate sqlSessionMasterReciplayUser;
	
	
	
	public int validateIngestedRecipe(long recipeId,long userId) {
		return sqlSessionMasterReciplayUser.getMapper(RecipeErrListDAO.class).validateIngestedRecipe( recipeId,userId);
		
	}
	

	
}
