package com.reciplay.cis.service;

import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reciplay.cis.common.Constants.WebConstant;
import com.reciplay.cis.common.ServiceException;
import com.reciplay.cis.dao.CookbookDAOService;
import com.reciplay.cis.dao.KeeperDAOService;
import com.reciplay.cis.dao.RecipeErrListDAOService;



@Service
public class AuthorizationService {	

	@Autowired
	CookbookDAOService cookbookDAOService;

	
	@Autowired
	KeeperDAOService keeperDAOService;

	@Autowired
	RecipeErrListDAOService recipeErrListDAOService;

	


	public void validateRecipeInfoForUser(long userId, long recipeId) {

		if (keeperDAOService.validateRecipeInfoForUser(userId, recipeId) == 0
				&& cookbookDAOService.validateUserCookBookRecipeSubscription(userId, recipeId) == 0) {
			throw new ServiceException(Status.UNAUTHORIZED, WebConstant.INVALID_USERID,
					WebConstant.INVALID_USERID_ERRCODE);
		}

	}
	

}
