package com.reciplay.cis.beans;

public class RecipeMonthStats {

	private long recipeId;
	private long currentMonthCount;
	private long previousMonthCount;
	private long total;
	private String recipeName;
	
	
	 
	public long getRecipeId() {
		return recipeId;
	}
	public void setRecipeId(long recipeId) {
		this.recipeId = recipeId;
	}
	public long getCurrentMonthCount() {
		return currentMonthCount;
	}
	public void setCurrentMonthCount(long currentMonthCount) {
		this.currentMonthCount = currentMonthCount;
	}
	public long getPreviousMonthCount() {
		return previousMonthCount;
	}
	public void setPreviousMonthCount(long previousMonthCount) {
		this.previousMonthCount = previousMonthCount;
	}
	
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	
	public String getRecipeName() {
		return recipeName;
	}
	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}
	public RecipeMonthStats() {
	}
	@Override
	public String toString() {
		return "RecipeMonthStats [recipeId=" + recipeId + ", currentMonthCount=" + currentMonthCount
				+ ", previousMonthCount=" + previousMonthCount + ", total=" + total + ", recipeName=" + recipeName
				+ "]";
	}
	public RecipeMonthStats(long recipeId, long currentMonthCount, long previousMonthCount, long total,
			String recipeName) {
		this.recipeId = recipeId;
		this.currentMonthCount = currentMonthCount;
		this.previousMonthCount = previousMonthCount;
		this.total = total;
		this.recipeName = recipeName;
	}
}
