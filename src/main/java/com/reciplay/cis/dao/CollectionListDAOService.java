package com.reciplay.cis.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.dao.TransientDataAccessException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import com.reciplay.cis.beans.CollectionBook;
import com.reciplay.cis.beans.RecipeData;

@Service
public class CollectionListDAOService {

	@Autowired
	SqlSessionTemplate sqlSessionMasterReciplayUser;

	@Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = 3, backoff = @Backoff(delay = 100))
	public List<CollectionBook> getCollected(long userId) {
				
		List<CollectionBook> collectionBookList = sqlSessionMasterReciplayUser.getMapper(CollectionListDAO.class).getAllCollections(userId);
		System.out.println("CollectionBookList "+collectionBookList.toString());
		return collectionBookList;
	}

	public Map<Long, RecipeData> getRecipeName(String allRecipeIds) {
		Map<Long,RecipeData> allRecipe = new HashMap<Long,RecipeData>();
		List<RecipeData> recipeData = getRecipeData(allRecipeIds);
		System.out.println("recipeData "+recipeData.toString());
		for(int a=0;a<recipeData.size();a++) {
			allRecipe.put(recipeData.get(a).getRecipeId(), recipeData.get(a));
		}
		return allRecipe;
	}
	
	@Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = 3, backoff = @Backoff(delay = 100))
	private List<RecipeData> getRecipeData(String allRecipeIds) {
		return sqlSessionMasterReciplayUser.getMapper(CollectionListDAO.class).getRecipeData(allRecipeIds);
	}

}