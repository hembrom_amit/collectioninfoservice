package com.reciplay.cis.webservice;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reciplay.cis.common.Constants.WebConstant;
import com.reciplay.cis.common.Constants;
import com.reciplay.cis.common.ErrorHelper;
import com.reciplay.cis.common.ServiceException;
import com.reciplay.cis.loggicaspect.LogginAspect;
import com.reciplay.cis.service.RecipeListService;

@Service
@Path("/recipelist")
public class RecipeListWebService {

	private static final Logger logger = LoggerFactory.getLogger(RecipeListWebService.class);

	@Autowired
	ErrorHelper errHelper;

	@Autowired
	RecipeListService recipeListService;

	@GET
	@Path("/getCollectionCB/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getinfo(@Context ContainerRequestContext crc,@PathParam("userId") long userId) throws ServiceException {

		logger.info("Request for user id " + userId);
		try {
            if (userId <= 0) {
            	throw new ServiceException(Status.BAD_REQUEST, WebConstant.PAYLOAD_NULL_OR_EMPTY,
                        WebConstant.PAYLOAD_NULL_OR_EMPTY_ERRCODE);
            }
            
            JSONObject jsonObject = (JSONObject) crc.getProperty(Constants.WebConstant.AUTHENTICATION_DATA);
			long userIdFromToken = jsonObject.getLong(Constants.WebConstant.USERID);

			if (userIdFromToken != userId) {
				throw new ServiceException(Status.UNAUTHORIZED, WebConstant.INVALID_USERID,
						WebConstant.INVALID_USERID_ERRCODE);
			}
			
            
            JSONArray collectionList = recipeListService.fetchAndReturnCollectionCB(userId);
            if (collectionList != null) {
                return Response.status(Status.OK).entity(collectionList.toString()).build();
            }
            throw new ServiceException(Status.INTERNAL_SERVER_ERROR, WebConstant.UNABLE_TO_PROCESS_REQUEST, WebConstant.UNABLE_TO_PROCESS_REQUEST_ERRCODE);
		}  catch (ServiceException se) {
			return Response.status(se.getStatus())
					.entity(errHelper.makeErrorCodeMap(se.getError(), se.getBusinessErrorCode(), se.getKeyValuemap()))
					.build();
		} catch (Exception ex) {
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(errHelper.makeErrorCode(WebConstant.UNABLE_TO_PROCESS_REQUEST,
							WebConstant.UNABLE_TO_PROCESS_REQUEST_ERRCODE))
					.build();
		}
	}
	
	@GET
	@Path("/getBrowsedRecipeList/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	@LogginAspect
	public Response getBrowsedRecipeList(@Context ContainerRequestContext crc,@PathParam("userId") long userId) throws ServiceException {

		logger.info("Request for user id " + userId);
		try {
            if (userId <= 0) {
            	throw new ServiceException(Status.BAD_REQUEST, WebConstant.PAYLOAD_NULL_OR_EMPTY,
                        WebConstant.PAYLOAD_NULL_OR_EMPTY_ERRCODE);
            }
            
            JSONObject jsonObject = (JSONObject) crc.getProperty(Constants.WebConstant.AUTHENTICATION_DATA);
			long userIdFromToken = jsonObject.getLong(Constants.WebConstant.USERID);

			if (userIdFromToken != userId) {
				throw new ServiceException(Status.UNAUTHORIZED, WebConstant.INVALID_USERID,
						WebConstant.INVALID_USERID_ERRCODE);
			}
			
            
            JSONArray collectionList = new JSONArray();// recipeListService.getBrowsedRecipeList(userId);
            if (collectionList != null) {
                return Response.status(Status.OK).entity(collectionList.toString()).build();
            }
            throw new ServiceException(Status.INTERNAL_SERVER_ERROR, WebConstant.UNABLE_TO_PROCESS_REQUEST, WebConstant.UNABLE_TO_PROCESS_REQUEST_ERRCODE);
		}  catch (ServiceException se) {
			return Response.status(se.getStatus())
					.entity(errHelper.makeErrorCodeMap(se.getError(), se.getBusinessErrorCode(), se.getKeyValuemap()))
					.build();
		} catch (Exception ex) {
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(errHelper.makeErrorCode(WebConstant.UNABLE_TO_PROCESS_REQUEST,
							WebConstant.UNABLE_TO_PROCESS_REQUEST_ERRCODE))
					.build();
		}
	}

	
}
