
CREATE TABLE `recipe_stats_table` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT, 
  `recipe_id` bigint(20) DEFAULT NULL,
  `browse_count_total` bigint(20) DEFAULT 0,
  `cooked_count_total` bigint(20) DEFAULT 0,
  `publish_count_total` bigint(20) DEFAULT 0,
  `comment_count_total` bigint(20) DEFAULT 0,
  `ask_chef_count_total` bigint(20) DEFAULT 0,
  `review_count_total` bigint(20) DEFAULT 0,
  `shared_count_total` bigint(20) DEFAULT 0,
  `chef_rating_average` bigint(20) DEFAULT 0,
  `recipe_rating_average` bigint(20) DEFAULT 0,
  `dish_rating_average` bigint(20) DEFAULT 0,
  `overall_rating` bigint(20) DEFAULT 0,
  `favorite_count_total` bigint(20) DEFAULT 0,
  `browsed_current_month` bigint(20) DEFAULT 0,
   `browsed_previous_month` bigint(20) DEFAULT 0,
  `askchef_current_month` bigint(20) DEFAULT 0,
   `askchef_previous_month` bigint(20) DEFAULT 0,
  `comment_current_month` bigint(20) DEFAULT 0,
  `comment_previous_month` bigint(20) DEFAULT 0,
  `collect_current_month` bigint(20) DEFAULT 0,
  `collect_previous_month` bigint(20) DEFAULT 0,
  `cooked_current_month` bigint(20) DEFAULT 0,
  `cooked_previous_month` bigint(20) DEFAULT 0,
  `favorite_current_month` bigint(20) DEFAULT 0,
  `favorite_previous_month` bigint(20) DEFAULT 0,
  `shared_current_month` bigint(20) DEFAULT 0,
  `shared_previous_month` bigint(20) DEFAULT 0,
  `last_updated` bigint(20) DEFAULT 0,
  `recipe_name` varchar(45) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_recipe` (`recipe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
 
CREATE TABLE `recipe_stats_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `recipe_id` bigint(20) DEFAULT NULL,
  `browse_count_total` bigint(20) DEFAULT '0',
  `cooked_count_total` bigint(20) DEFAULT '0',
  `publish_count_total` bigint(20) DEFAULT '0',
  `comment_count_total` bigint(20) DEFAULT '0',
  `ask_chef_count_total` bigint(20) DEFAULT '0',
  `review_count_total` bigint(20) DEFAULT '0',
  `shared_count_total` bigint(20) DEFAULT '0',
  `chef_rating_average` bigint(20) DEFAULT '0',
  `recipe_rating_average` bigint(20) DEFAULT '0',
  `dish_rating_average` bigint(20) DEFAULT '0',
  `overall_rating` bigint(20) DEFAULT '0',
  `favorite_count_total` bigint(20) DEFAULT '0',
  `browsed_current_month` bigint(20) DEFAULT '0',
  `browsed_previous_month` bigint(20) DEFAULT '0',
  `askchef_current_month` bigint(20) DEFAULT '0',
  `askchef_previous_month` bigint(20) DEFAULT '0',
  `comment_current_month` bigint(20) DEFAULT '0',
  `comment_previous_month` bigint(20) DEFAULT '0',
  `collect_current_month` bigint(20) DEFAULT '0',
  `collect_previous_month` bigint(20) DEFAULT '0',
  `cooked_current_month` bigint(20) DEFAULT '0',
  `cooked_previous_month` bigint(20) DEFAULT '0',
  `favorite_current_month` bigint(20) DEFAULT '0',
  `favorite_previous_month` bigint(20) DEFAULT '0',
  `shared_current_month` bigint(20) DEFAULT '0',
  `shared_previous_month` bigint(20) DEFAULT '0',
  `last_updated` bigint(20) DEFAULT '0', 
  `review_current_month` bigint(20) DEFAULT '0',
  `review_previous_month` bigint(20) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_recipe` (`recipe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `monthly_recipe_stats_table` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT, 
  `recipe_id` bigint(20) DEFAULT NULL,
  
  `last_updated` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_recipe` (`recipe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


CREATE TABLE `recipe_stats_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `recipe_id` bigint(20) DEFAULT NULL,
  `browsed_count` bigint(20) DEFAULT NULL,
  `askchef_count` bigint(20) DEFAULT NULL,
  `message_count` bigint(20) DEFAULT NULL,
  `collect_count` bigint(20) DEFAULT NULL,
  `cook_count` bigint(20) DEFAULT NULL,
  `share_count` bigint(20) DEFAULT NULL,
  `month` int(4) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `last_updated` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_recipe` (`recipe_id`,`month`,`year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
   
CREATE TABLE `user_interim_stats` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `currentStats` text  DEFAULT NULL,
  `previousStats` text DEFAULT NULL,
  `last_view_time` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
   UNIQUE KEY `uk_recipe` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
 
CREATE TABLE `user_favorite_recipe_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `recipe_id` bigint(20) DEFAULT NULL,
  `browsed_id` bigint(20) DEFAULT NULL,
  `cooked_id` bigint(20) DEFAULT NULL,
  `review_id` bigint(20) DEFAULT NULL,
  `ask_chef_id` bigint(20) DEFAULT NULL,
  `last_updated` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

ALTER TABLE user_favorite_recipe_info ADD UNIQUE KEY `uk_user_recipe` (user_id, recipe_id);

CREATE TABLE `action_favorite` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `recipe_id` bigint(20) DEFAULT NULL,
  `timestamp` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1

ALTER TABLE action_favorite ADD UNIQUE KEY `uk_user_recipe` (user_id, recipe_id);


CREATE TABLE `user_recipe_published_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `recipe_id` bigint(20) DEFAULT NULL,
  `publish_date` bigint(20) DEFAULT NULL,
  `is_published` bigint(20) DEFAULT NULL,
  `is_deleted` bigint(20) DEFAULT '0',
  `recipe_name` varchar(200) DEFAULT "",
  `last_updated` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_recipe` (`recipe_id`),
  UNIQUE KEY `uk_user_recipe` (`user_id`,`recipe_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8


CREATE TABLE `user_recipe_info` (
       `id` bigint(20) NOT NULL AUTO_INCREMENT,
       `mongo_id` varchar(200) DEFAULT '',
       `user_id` bigint(20) DEFAULT NULL,
       `recipe_id` bigint(20) DEFAULT NULL,
       `created_date` bigint(20) DEFAULT NULL,
       `publish_date` bigint(20) DEFAULT NULL,
       `is_published` bigint(20) DEFAULT '0',
       `is_deleted` bigint(20) DEFAULT '0',
       `last_updated` bigint(20) DEFAULT NULL,
       `recipe_name` varchar(200) DEFAULT '',
       PRIMARY KEY (`id`),
       UNIQUE KEY `uk_recipe` (`recipe_id`),
       UNIQUE KEY `uk_user_recipe` (`user_id`,`recipe_id`)
     ) ENGINE=InnoDB DEFAULT CHARSET=utf8;