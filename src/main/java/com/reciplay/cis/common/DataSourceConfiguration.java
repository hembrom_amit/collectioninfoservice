
/**
 * 
 */
package com.reciplay.cis.common;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class to load bean for master data source. Uses configuration
 * properties from either zookeeper or json property file.
 */
@Configuration
public class DataSourceConfiguration {

	private static final Logger logger = LoggerFactory.getLogger(DataSourceConfiguration.class);

	@PostConstruct
	public void init(){
		logger.info("Starting the DB connection : ");
		System.out.println("Starting the DB connection : ");
		getMasterDataSource();
		getMasterDataSourceReciplayUser();
		logger.info("Done DB connection : ");
		System.out.println("Done DB connection : ");
	}

    /**
     * Method to load a bean for master data source using configuration
     * properties.
     * 
     * @return
     */
    @Bean(name = { "masterDataSource" })
    public DataSource getMasterDataSource() {
        BasicDataSource master = new BasicDataSource();
        master.setDriverClassName(  "com.mysql.jdbc.Driver");
        master.setUsername( "root");
        master.setPassword( "root1234");
        master.setUrl("jdbc:mysql://localhost:3306/reciplay");
        return master;
    }
    
    /**
     * Method to load a bean for master data source using configuration
     * properties.
     * 
     * @return
     */
    @Bean(name = { "masterDataSourceReciplayUser" })
    public DataSource getMasterDataSourceReciplayUser() {
        BasicDataSource master = new BasicDataSource();
        master.setDriverClassName(  "com.mysql.jdbc.Driver");
        master.setUsername( "root");
        master.setPassword( "root1234");
        master.setUrl("jdbc:mysql://localhost:3306/reciplayuser");
        return master;
    }
    

}