package com.reciplay.cis.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class CookbookDAOService {

	@Autowired
	SqlSessionTemplate sqlSessionMasterReciplayUser;
	
	@Autowired
	SqlSessionTemplate sqlSessionMaster;
	
	
	public int checkIfUnique(String fullURL) {
		return sqlSessionMasterReciplayUser.getMapper(CookbookDAO.class).checkIfUnique(fullURL);
		
	}
	
	public int checkIfUniqueRecipe(String fullURL) {
		return sqlSessionMasterReciplayUser.getMapper(CookbookDAO.class).checkIfUniqueRecipe(fullURL);
		
	}
	
	public int validateCookBookSubUser(Long userId,long cbId) {
		return sqlSessionMasterReciplayUser.getMapper(CookbookDAO.class).validateCookBookSubUser(userId,cbId);
		
	}
	
	public int validateCookBookEntityUser(Long userId,long cbId) {
		return sqlSessionMasterReciplayUser.getMapper(CookbookDAO.class).validateCookBookEntityUser(userId,cbId);
		
	}
	
	public int validateCookbookSubscriptionUsers(Long userId,String cbUnique) {
		return sqlSessionMasterReciplayUser.getMapper(CookbookDAO.class).validateCookbookSubscriptionUsers(userId,cbUnique);
		
	}
	public int validateUserCookBookRecipeSubscription(Long userLong,Long recipeId) {
		return sqlSessionMasterReciplayUser.getMapper(CookbookDAO.class).validateUserCookBookRecipeSubscription(userLong,recipeId);
		
	}
	
	
}
