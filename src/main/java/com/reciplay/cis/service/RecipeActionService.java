package com.reciplay.cis.service;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.reciplay.cis.beans.IntermStatsData;
import com.reciplay.cis.beans.PayloadInfo;
import com.reciplay.cis.beans.RecipeInfo;
import com.reciplay.cis.beans.RecipeInfoVO;
import com.reciplay.cis.beans.RecipeMonthStats;
import com.reciplay.cis.beans.RecipeStatsVO;
import com.reciplay.cis.beans.User;
import com.reciplay.cis.beans.UserRecipeStatsInfo;
import com.reciplay.cis.common.CISUtility;
import com.reciplay.cis.common.Constants.StatsConstant;
import com.reciplay.cis.common.Constants.WebConstant;
import com.reciplay.cis.common.ServiceException;
import com.reciplay.cis.dao.RecipeActionDAOService;
import com.reciplay.cis.dao.UserDAOService;

@Service
public class RecipeActionService {

	private static final Logger logger = LoggerFactory.getLogger(RecipeActionService.class);

	@Autowired
	RecipeActionDAOService recipeActionDAOService;

	@Autowired
	PublishRecipeService publishRecipeService;
	
	@Autowired
	UserDAOService userDAOService;
	
	@Autowired
	CISUtility util;
 
	public PayloadInfo convertPayloadToBrowseInfo(JSONObject jsonObject) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		PayloadInfo browseInfo = null;
		try {
			browseInfo = mapper.readValue(jsonObject.toString(), PayloadInfo.class);
		} catch (Exception ex) {
			throw new ServiceException(Status.BAD_REQUEST, WebConstant.IMPROPER_PAYLOAD,
					WebConstant.IMPROPER_PAYLOAD_ERRCODE, ex);
		}

		validateInput(browseInfo);
		logger.info("BrowseInfo POJO " + browseInfo.toString());
		return browseInfo;
	}

	public void validateInput(PayloadInfo browseInfo) {
		if (browseInfo.getUserId() <= 0) {
			throw new ServiceException(Status.BAD_REQUEST, WebConstant.INVALID_USERID,
					WebConstant.INVALID_USERID_ERRCODE);
		}
		if (browseInfo.getRecipeId() <= 0) {
			throw new ServiceException(Status.BAD_REQUEST, WebConstant.INVALID_RECIPEID,
					WebConstant.INVALID_RECIPEID_ERRCODE);
		}
		if (browseInfo.getCookbookId() <= 0) {
			throw new ServiceException(Status.BAD_REQUEST, WebConstant.INVALID_COOKBOOKID,
					WebConstant.INVALID_COOKBOOKID_ERRCODE);
		}
		
	}

	public JSONObject getPublishedRecipe(long userId, int sortBy,String newlist, int page,int recipePerPage) {

		JSONObject json = new JSONObject();
		JSONObject collectionInfo = new JSONObject();
		JSONArray publishedRecipeList = new JSONArray();
		JSONObject recipeStatsData = new JSONObject();
		String tabSelected="browse";
		if(newlist!=null) {
			tabSelected=newlist;
		}
		if(recipePerPage==0){
			recipePerPage=10;
		}
		logger.info("userId ="+userId+" sortBy "+sortBy+" newlist ="+newlist+" page = "+page+" Recipe per page = "+recipePerPage);
		if(page==0 || page==1) {
			page =0;
		} else {
			page=page-1;
		}
		getCollectionInfo(userId, collectionInfo, recipeStatsData);
		String chefName="";
		if(collectionInfo.has("chefName")) {
			chefName = collectionInfo.getString("chefName");
		} else {
			chefName ="Author_Chefname";
		}
		List<RecipeMonthStats> allRecipeStats=new ArrayList<RecipeMonthStats>();
		List<RecipeInfo> rcList = recipeActionDAOService.getAllPublishedRecipes(userId,page);
		if (rcList != null && !rcList.isEmpty()) {
			String allRecipeStr = getRecipeStringValue(rcList);
			logger.info("NEW list = "+newlist);
			allRecipeStats = getRecipeStatsList(sortBy,allRecipeStr,page,newlist,recipePerPage);
			for(int a =0;a<allRecipeStats.size();a++) {
				System.out.println(" Order = "+allRecipeStats.get(a).toString());
			}
			logger.info("collectionInfo "+collectionInfo.toString());
			prepareRecipeList( publishedRecipeList , allRecipeStats , rcList ,  chefName,newlist ,recipePerPage);
		}
		json.put("collectionInfo", collectionInfo);
		json.put("recipestats", recipeStatsData);
		json.put("recipelist", publishedRecipeList);
		json.put("tabselected", tabSelected);
		if (allRecipeStats.size() > recipePerPage) {
			json.put("hasMoreRecipe", true);
		} else {
			json.put("hasMoreRecipe", false);
		}
		return json;
	}

  List<RecipeMonthStats> getRecipeStatsList(int sortBy, String allRecipeStr, int page,String newlist,int recipePerPage) {
	  List<RecipeMonthStats> allRecipeStats = null;
	  if( sortBy ==StatsConstant.SORT_BY_PUBLISHED_DATE ) {
		  if("cook".equalsIgnoreCase(newlist)) {
			  allRecipeStats= recipeActionDAOService.getAllPublishedRecipesByPublishedDateCook(allRecipeStr,page ,recipePerPage);
		  } else {
			  allRecipeStats= recipeActionDAOService.getAllPublishedRecipesByPublishedDate(allRecipeStr,page,recipePerPage);
		  }
			
		} else {
			 if("cook".equalsIgnoreCase(newlist)) {
				 allRecipeStats= recipeActionDAOService.getRecipeStatsCookByRecipeStr(allRecipeStr,sortBy,page,recipePerPage);
			  } else {
				  allRecipeStats= recipeActionDAOService.getRecipeStatsBrowsedByRecipeStr(allRecipeStr,sortBy,page,recipePerPage);
			  }
		}
	  return allRecipeStats;
	}

	public HashMap<Long,RecipeMonthStats> recipeStatsAsMap(List<RecipeMonthStats> allRecipeStats) {
		HashMap<Long,RecipeMonthStats> recipeStats = new HashMap<>();
		for(int i=0;i<allRecipeStats.size();i++) {
			recipeStats.put(allRecipeStats.get(i).getRecipeId() , allRecipeStats.get(i));
		}
		return recipeStats;
	}
	public HashMap<Long,RecipeInfo> recipeListAsMap(List<RecipeInfo> allRecipeStats) {
		HashMap<Long,RecipeInfo> recipeStats = new HashMap<>();
		for(int i=0;i<allRecipeStats.size();i++) {
			recipeStats.put(allRecipeStats.get(i).getRecipeId() , allRecipeStats.get(i));
		}
		return recipeStats;
	}
	public String getRecipeStringValue(List<RecipeInfo> rcList) {
		StringBuilder str = new StringBuilder();
		for(int i=0;i<rcList.size();i++) {
			if(str.length()>0) {
				str.append(","+rcList.get(i).getRecipeId());
			} else {
				str.append(rcList.get(i).getRecipeId()+"");
			}
		}
		return str.toString();
	}

	public List<RecipeInfo> prepareRecipeList(List<RecipeInfo> rcList, JSONArray json,int recipePerPage) {
		List<RecipeInfo> recipeInfoList = new ArrayList<>();
		int count = rcList.size() > recipePerPage ? recipePerPage : rcList.size();
		for (int i = 0; i < count; i++) {
			JSONObject jvalue = new JSONObject();
			RecipeInfo info = new RecipeInfo();
			info.setRecipeId(rcList.get(i).getRecipeId());
			jvalue.put("recipeId", rcList.get(i).getRecipeId());
			info.setRecipeName(rcList.get(i).getRecipeId() + " - Recipe");
			jvalue.put("recipeName", rcList.get(i).getRecipeId() + " - Recipe");
			info.setRecipeImage("https://goo.gl/FqwtQY");
			jvalue.put("reciepeImg", "https://goo.gl/FqwtQY");
			jvalue.put("reviewStar", getRandomNumber(1, 4));
			jvalue.put("reviewRating", getRandomNumber(1, 5));
			jvalue.put("reviewCount", getRandomNumber(10, 400));
			jvalue.put("recipeMetaData", "Indian main course - Veg");
			recipeInfoList.add(info);
			json.put(jvalue);
		}
		return recipeInfoList;
	}

	public void prepareRecipeList(JSONArray publishedRecipeList,
			List<RecipeMonthStats> recipeList , List<RecipeInfo> rcList ,String chefName ,String newlist,int recipePerPage) {
		HashMap<Long,RecipeMonthStats>  recipeMap = recipeStatsAsMap(recipeList);
		HashMap<Long,RecipeInfo>  rcListMap = recipeListAsMap(rcList);
		String statsPrefix = getStatsPrefix(newlist);
		int count = recipeList.size() > recipePerPage ? recipePerPage : recipeList.size();
		for (int i = 0; i < count; i++) {
			logger.info("First element = "+recipeList.get(i).toString());
			System.out.println(i+" ==> "+recipeList.get(i).toString());
			JSONObject jvalue = new JSONObject();
			RecipeInfo info = new RecipeInfo();
			info.setRecipeId(recipeList.get(i).getRecipeId() );
			jvalue.put("recipeId", recipeList.get(i).getRecipeId());
			info.setRecipeName(recipeList.get(i).getRecipeId() + " - Recipe");
			jvalue.put("recipeName", recipeList.get(i).getRecipeName());
			info.setRecipeImage("https://goo.gl/FqwtQY");
			jvalue.put("chefName", chefName);
			jvalue.put("reciepeImg", "https://goo.gl/FqwtQY");
			jvalue.put("reviewStar", getRandomNumber(1, 5));
			jvalue.put("reviewRating", getRandomNumber(1, 5));
			jvalue.put("reviewCount", getRandomNumber(10, 400));
			jvalue.put("displayType", "monthlyMatrix");
			if(rcListMap.containsKey(recipeList.get(i).getRecipeId())) {
				jvalue.put("publishedDate", getCreationTime(rcListMap.get(recipeList.get(i).getRecipeId()).getPublishedTime()) );
			}
			
			if(recipeMap.containsKey(recipeList.get(i).getRecipeId())) {
				LocalDate currentDate = LocalDate.now();
				String currentMonth = util.getCurrentMonth(currentDate);
				String previousMonth = util.getPreviousMonth(currentDate);
				/*jvalue.put("recipeStatsInfo", statsPrefix+""+currentMonth+"-"+
						recipeMap.get(recipeList.get(i).getRecipeId()).getCurrentMonthCount()+" "+previousMonth+"-"+
						recipeMap.get(recipeList.get(i).getRecipeId()).getPreviousMonthCount()+" Tot-"+recipeMap.get(recipeList.get(i).getRecipeId()).getTotal());*/
				JSONObject jsonStat = new JSONObject();
				jsonStat.put("currrentMonth", currentMonth);
				jsonStat.put("previousMonth", previousMonth);
				jsonStat.put("currrentCount", recipeMap.get(recipeList.get(i).getRecipeId()).getCurrentMonthCount());
				jsonStat.put("previousCount", recipeMap.get(recipeList.get(i).getRecipeId()).getPreviousMonthCount());
				jsonStat.put("totalCount", recipeMap.get(recipeList.get(i).getRecipeId()).getTotal());
				
				jvalue.put("displayInfo", jsonStat);
			}
			
			
			publishedRecipeList.put(jvalue);
		}
	}

	private String getStatsPrefix(String newlist) {
		String prefix="";
		if(newlist==null) {
			prefix="Browsed - ";
		} else if(newlist.equalsIgnoreCase("cook")) {
			prefix="Cook - ";
		}else if(newlist.equalsIgnoreCase("askchef")) {
			prefix="Askchef - ";
		}else if(newlist.equalsIgnoreCase("like")) {
			prefix="Favorite - ";
		}else if(newlist.equalsIgnoreCase("review")) {
			prefix="Review - ";
		}else if(newlist.equalsIgnoreCase("share")) {
			prefix="Share - ";
		}
		return prefix;
	}

	private int getRandomNumber() {
		Random r = new Random();
		int Low = 1;
		int High = 5;
		int result = r.nextInt(High - Low) + Low;
		return result;
	}

	private int getRandomNumber(int low, int high) {
		Random r = new Random();
		int result = r.nextInt(high - low) + low;
		return result;
	}

	public void getCollectionInfo(long userId, JSONObject collectionInfo, JSONObject recipeStatsData) {
		User userInfo = userDAOService.getUserData(userId);
		if (userInfo != null) {
			collectionInfo.put("chefName", userInfo.getFirstname() + " " + userInfo.getLastname());
		}
		UserRecipeStatsInfo userStats= getStatsForTheUser(userId);
		if(userStats!=null) {
			JSONObject actionBrowse = new JSONObject();
			actionBrowse.put("count", userStats.getBrowseCount() );
			RecipeStatsVO checkNewStats = getNewStatsSinceLastLogin(userId);
			actionBrowse.put("new", checkNewStats.isNewBrowse());
			recipeStatsData.put("browse", actionBrowse);
			
			JSONObject actionCook = new JSONObject();
			actionCook.put("count", userStats.getCookCount() );
			actionCook.put("new", checkNewStats.isNewCook());
			recipeStatsData.put("cook", actionCook);
			
			
			JSONObject actionLike = new JSONObject();
			actionLike.put("count", userStats.getFavoriteCount() );
			actionLike.put("new", checkNewStats.isNewFavorite());
			recipeStatsData.put("like", actionLike);
			
			
			JSONObject actionShare = new JSONObject();
			actionShare.put("count",userStats.getSharedCount() );
			actionShare.put("new", checkNewStats.isNewShared());
			recipeStatsData.put("share", actionShare);
			
			
			JSONObject actionMsg = new JSONObject();
			actionMsg.put("count",userStats.getCommentCount() );
			actionMsg.put("new", checkNewStats.isNewComment());
			recipeStatsData.put("message", actionMsg);
			
			
			JSONObject actionAskChef = new JSONObject();
			actionAskChef.put("count", userStats.getAskChefCount() );
			actionAskChef.put("new", checkNewStats.isNewAskChef());
			recipeStatsData.put("askchef", actionAskChef);
			

		}
		
		RecipeInfoVO recipeInfoVO = recipeActionDAOService.getTotalPublishedRecipeCount(userId);
		collectionInfo.put("totalRecipeCount", (int) recipeInfoVO.getRecipeCount());
		if (recipeInfoVO.getLastPublishedTime() > 0) {
			collectionInfo.put("creationTime", getCreationTime(recipeInfoVO.getLastPublishedTime()));
		}
	}

	public RecipeStatsVO getNewStatsSinceLastLogin(long userId) {
		IntermStatsData newStats = recipeActionDAOService.getUserInterimStatsData(userId);
		if(newStats==null) {
			recipeActionDAOService.insertIntermStatsData(userId, new IntermStatsData());
			return new RecipeStatsVO();
		} else {
			return updateIfNewStats(newStats);
		}
		
	}

	private RecipeStatsVO updateIfNewStats(IntermStatsData newStats) {
		logger.info("Checking == ");
		String currentData = newStats.getRunningStatsData();
		String previousData = newStats.getPreviousStatsData();
		if(previousData==null && currentData==null) {
			return new RecipeStatsVO();
		} else if(previousData==null && currentData!=null) {
			return getNonEmptyCount(currentData);
		}
		return compareTwoStats(previousData,currentData);
	}

	public RecipeStatsVO compareTwoStats(String previousData, String currentData) {

		UserRecipeStatsInfo statsPrevious = util.convertStringToRecipeStatsInfo(new JSONObject(previousData));
		UserRecipeStatsInfo statsCurrent = util.convertStringToRecipeStatsInfo(new JSONObject(currentData));
		RecipeStatsVO recipeVO = new RecipeStatsVO();
		if(statsPrevious!=null && statsCurrent!=null) {
			if(statsCurrent.getAskChefCount()> statsPrevious.getAskChefCount()) {
				recipeVO.setNewAskChef(true);
			}
			if(statsCurrent.getBrowseCount()> statsPrevious.getBrowseCount()) {
				recipeVO.setNewBrowse(true);
			}
			if(statsCurrent.getCommentCount()> statsPrevious.getCommentCount()) {
				recipeVO.setNewComment(true);
			}
			if(statsCurrent.getCookCount() > statsPrevious.getCookCount()) {
				recipeVO.setNewCook(true);
			}
			if(statsCurrent.getFavoriteCount()> statsPrevious.getFavoriteCount()) {
				recipeVO.setNewFavorite(true);
			}
			if(statsCurrent.getSharedCount() > statsPrevious.getSharedCount()) {
				recipeVO.setNewShared(true);
			}
		}
		return recipeVO;
	}

	private RecipeStatsVO getNonEmptyCount(String currentData) {
		UserRecipeStatsInfo userRecipeStats = util.convertStringToRecipeStatsInfo(new JSONObject(currentData));
		RecipeStatsVO recipeVO = new RecipeStatsVO();
		if(userRecipeStats!=null) {
			if(userRecipeStats.getAskChefCount()>0) {
				recipeVO.setNewAskChef(true);
			}
			if(userRecipeStats.getBrowseCount() >0) {
				recipeVO.setNewBrowse(true);
			}
			if(userRecipeStats.getCommentCount() >0) {
				recipeVO.setNewComment(true);
			}
			if(userRecipeStats.getCookCount() >0) {
				recipeVO.setNewCook(true);
			}
			if(userRecipeStats.getFavoriteCount() >0) {
				recipeVO.setNewFavorite(true);
			}
			if(userRecipeStats.getSharedCount() >0) {
				recipeVO.setNewShared(true);
			}
		}
		return recipeVO;
	}

	public UserRecipeStatsInfo getStatsForTheUser(long userId) {

		List<Long> recipeIds = recipeActionDAOService.getAllPublishedRecipeIds(userId);
		if(recipeIds!=null && !recipeIds.isEmpty()) {
			String allRecipeStr = getRecipeString(recipeIds);
			return recipeActionDAOService.getTotalBrowseCountByRecipeIds(allRecipeStr);
		}
		return null;
	}

	public String getRecipeString(List<Long> recipeIds) {
		StringBuilder str = new StringBuilder();
		/*recipeIds.forEach(recipeid->{
			if(str.length()>0) {
				str.append(","+recipeid);
			} else {
				str.append(recipeid+"");
			}
		});*/
		for(int i=0;i<recipeIds.size();i++) {
			if(str.length()>0) {
				str.append(","+recipeIds.get(i));
			} else {
				str.append(recipeIds.get(i)+"");
			}
		}
		return str.toString();
	}

	private String getCreationTime(long lastPublishedTime) {
		Date date = new Date(lastPublishedTime);
		SimpleDateFormat df2 = new SimpleDateFormat("dd MMM yyyy");
		String dateText = df2.format(date);
		return dateText;
	}

	public static void main(String[] args) {
		long lastPublishedTime=System.currentTimeMillis();
		
		Date date = new Date(lastPublishedTime);
		SimpleDateFormat df2 = new SimpleDateFormat("dd MMM yyyy");
		String dateText = df2.format(date);
		System.out.println("Date :: "+date+" : "+dateText);
	}

}
