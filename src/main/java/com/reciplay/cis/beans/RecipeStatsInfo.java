package com.reciplay.cis.beans;

public class RecipeStatsInfo {

	private long recipe_id;
	private long browse_count_total;
	private long cooked_count_total;
	private long publish_count_total;
	private long comment_count_total;
	private long ask_chef_count_total;
	private long review_count_total;
	private long shared_count_total;
	
	private long chef_rating_average;
	private long recipe_rating_average;
	private long dish_rating_average;
	private long overall_rating;
	private long favorite_count_total;
	
	private long browsed_current_month;
	private long browsed_previous_month;
	private long askchef_current_month;
	private long askchef_previous_month;
	private long comment_current_month;
	
	private long comment_previous_month;
	private long collect_current_month;
	private long collect_previous_month;
	private long cooked_current_month;
	private long cooked_previous_month;
	
	private long favorite_current_month;
	private long favorite_previous_month;
	private long shared_current_month;
	private long shared_previous_month;
	private long last_updated;
	
	private long review_current_month;
	private long review_previous_month;
	
	private String recipeName;
	
	
	
	public long getRecipe_id() {
		return recipe_id;
	}
	public void setRecipe_id(long recipe_id) {
		this.recipe_id = recipe_id;
	}
	public long getBrowse_count_total() {
		return browse_count_total;
	}
	public void setBrowse_count_total(long browse_count_total) {
		this.browse_count_total = browse_count_total;
	}
	public long getCooked_count_total() {
		return cooked_count_total;
	}
	public void setCooked_count_total(long cooked_count_total) {
		this.cooked_count_total = cooked_count_total;
	}
	public long getPublish_count_total() {
		return publish_count_total;
	}
	public void setPublish_count_total(long publish_count_total) {
		this.publish_count_total = publish_count_total;
	}
	public long getComment_count_total() {
		return comment_count_total;
	}
	public void setComment_count_total(long comment_count_total) {
		this.comment_count_total = comment_count_total;
	}
	public long getAsk_chef_count_total() {
		return ask_chef_count_total;
	}
	public void setAsk_chef_count_total(long ask_chef_count_total) {
		this.ask_chef_count_total = ask_chef_count_total;
	}
	public long getReview_count_total() {
		return review_count_total;
	}
	public void setReview_count_total(long review_count_total) {
		this.review_count_total = review_count_total;
	}
	public long getShared_count_total() {
		return shared_count_total;
	}
	public void setShared_count_total(long shared_count_total) {
		this.shared_count_total = shared_count_total;
	}
	public long getChef_rating_average() {
		return chef_rating_average;
	}
	public void setChef_rating_average(long chef_rating_average) {
		this.chef_rating_average = chef_rating_average;
	}
	public long getRecipe_rating_average() {
		return recipe_rating_average;
	}
	public void setRecipe_rating_average(long recipe_rating_average) {
		this.recipe_rating_average = recipe_rating_average;
	}
	public long getDish_rating_average() {
		return dish_rating_average;
	}
	public void setDish_rating_average(long dish_rating_average) {
		this.dish_rating_average = dish_rating_average;
	}
	public long getOverall_rating() {
		return overall_rating;
	}
	public void setOverall_rating(long overall_rating) {
		this.overall_rating = overall_rating;
	}
	public long getFavorite_count_total() {
		return favorite_count_total;
	}
	public void setFavorite_count_total(long favorite_count_total) {
		this.favorite_count_total = favorite_count_total;
	}
	public long getBrowsed_current_month() {
		return browsed_current_month;
	}
	public void setBrowsed_current_month(long browsed_current_month) {
		this.browsed_current_month = browsed_current_month;
	}
	public long getBrowsed_previous_month() {
		return browsed_previous_month;
	}
	public void setBrowsed_previous_month(long browsed_previous_month) {
		this.browsed_previous_month = browsed_previous_month;
	}
	public long getAskchef_current_month() {
		return askchef_current_month;
	}
	public void setAskchef_current_month(long askchef_current_month) {
		this.askchef_current_month = askchef_current_month;
	}
	public long getAskchef_previous_month() {
		return askchef_previous_month;
	}
	public void setAskchef_previous_month(long askchef_previous_month) {
		this.askchef_previous_month = askchef_previous_month;
	}
	public long getComment_current_month() {
		return comment_current_month;
	}
	public void setComment_current_month(long comment_current_month) {
		this.comment_current_month = comment_current_month;
	}
	public long getComment_previous_month() {
		return comment_previous_month;
	}
	public void setComment_previous_month(long comment_previous_month) {
		this.comment_previous_month = comment_previous_month;
	}
	public long getCollect_current_month() {
		return collect_current_month;
	}
	public void setCollect_current_month(long collect_current_month) {
		this.collect_current_month = collect_current_month;
	}
	public long getCollect_previous_month() {
		return collect_previous_month;
	}
	public void setCollect_previous_month(long collect_previous_month) {
		this.collect_previous_month = collect_previous_month;
	}
	public long getCooked_current_month() {
		return cooked_current_month;
	}
	public void setCooked_current_month(long cooked_current_month) {
		this.cooked_current_month = cooked_current_month;
	}
	public long getCooked_previous_month() {
		return cooked_previous_month;
	}
	public void setCooked_previous_month(long cooked_previous_month) {
		this.cooked_previous_month = cooked_previous_month;
	}
	public long getFavorite_current_month() {
		return favorite_current_month;
	}
	public void setFavorite_current_month(long favorite_current_month) {
		this.favorite_current_month = favorite_current_month;
	}
	public long getFavorite_previous_month() {
		return favorite_previous_month;
	}
	public void setFavorite_previous_month(long favorite_previous_month) {
		this.favorite_previous_month = favorite_previous_month;
	}
	public long getShared_current_month() {
		return shared_current_month;
	}
	public void setShared_current_month(long shared_current_month) {
		this.shared_current_month = shared_current_month;
	}
	public long getShared_previous_month() {
		return shared_previous_month;
	}
	public void setShared_previous_month(long shared_previous_month) {
		this.shared_previous_month = shared_previous_month;
	}
	public long getLast_updated() {
		return last_updated;
	}
	public void setLast_updated(long last_updated) {
		this.last_updated = last_updated;
	}
	public long getReview_current_month() {
		return review_current_month;
	}
	public void setReview_current_month(long review_current_month) {
		this.review_current_month = review_current_month;
	}
	public long getReview_previous_month() {
		return review_previous_month;
	}
	public void setReview_previous_month(long review_previous_month) {
		this.review_previous_month = review_previous_month;
	}
	
	public String getRecipeName() {
		return recipeName;
	}
	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}
	@Override
	public String toString() {
		return "RecipeStatsInfo [recipe_id=" + recipe_id + ", browse_count_total=" + browse_count_total
				+ ", cooked_count_total=" + cooked_count_total + ", publish_count_total=" + publish_count_total
				+ ", comment_count_total=" + comment_count_total + ", ask_chef_count_total=" + ask_chef_count_total
				+ ", review_count_total=" + review_count_total + ", shared_count_total=" + shared_count_total
				+ ", chef_rating_average=" + chef_rating_average + ", recipe_rating_average=" + recipe_rating_average
				+ ", dish_rating_average=" + dish_rating_average + ", overall_rating=" + overall_rating
				+ ", favorite_count_total=" + favorite_count_total + ", browsed_current_month=" + browsed_current_month
				+ ", browsed_previous_month=" + browsed_previous_month + ", askchef_current_month="
				+ askchef_current_month + ", askchef_previous_month=" + askchef_previous_month
				+ ", comment_current_month=" + comment_current_month + ", comment_previous_month="
				+ comment_previous_month + ", collect_current_month=" + collect_current_month
				+ ", collect_previous_month=" + collect_previous_month + ", cooked_current_month="
				+ cooked_current_month + ", cooked_previous_month=" + cooked_previous_month
				+ ", favorite_current_month=" + favorite_current_month + ", favorite_previous_month="
				+ favorite_previous_month + ", shared_current_month=" + shared_current_month
				+ ", shared_previous_month=" + shared_previous_month + ", last_updated=" + last_updated
				+ ", review_current_month=" + review_current_month + ", review_previous_month=" + review_previous_month
				+ ", recipeName=" + recipeName + "]";
	}
	 
}
