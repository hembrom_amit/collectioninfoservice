package com.reciplay.cis.common;

import java.util.HashMap;

import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.reciplay.cis.common.Constants.WebConstant;

@Component
public class ErrorHelper {
    
    public String makeErrorCode(String invalidJson, int invalidJsonErrcode) {
        JSONObject json=new JSONObject();
        json.put(WebConstant.ERROR_MESSAGE, invalidJson);
        json.put(WebConstant.ERROR_CODE, invalidJsonErrcode);
        return json.toString();
    }
    
    public String makeErrorCodeMap(String invalidJson, int invalidJsonErrcode,HashMap<String,Object> map) {
        JSONObject json=new JSONObject();
        json.put(WebConstant.ERROR_MESSAGE, invalidJson);
        json.put(WebConstant.ERROR_CODE, invalidJsonErrcode);
        if(map!=null) {
            for(String key:map.keySet()) {
            	json.put(key, map.get(key));
            }
        }

        return json.toString();
    }
     
}
