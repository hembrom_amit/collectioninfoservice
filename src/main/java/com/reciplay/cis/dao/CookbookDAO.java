package com.reciplay.cis.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface CookbookDAO {

	
	@Select("select count(1) from cookbook_info  where  unique_cb_value=#{fullURL}")
	int checkIfUnique(@Param("fullURL") String fullURL);

	@Select("select count(1) from cookbook_recipe_map  where  unique_value=#{fullURL}")
	int checkIfUniqueRecipe(@Param("fullURL") String fullURL);

	@Select("select count(1) from cookbook_subscriber  where user_id=#{userId} and  cookbook_id=#{cookbookId}")
	int validateCookBookSubUser(@Param("userId") Long userId, @Param("cookbookId") long cbId);

	@Select("select count(1) from cookbook_info cbi,entity_info ei where cbi.entity_id=ei.id and ei.user_id=#{userId} and cbi.id=#{cookbookId}")
	int validateCookBookEntityUser(@Param("userId") Long userId, @Param("cookbookId") long cbId);

	@Select("select count(1) from cookbook_reader cbr,cookbook_info cbi  where  cbi.unique_cb_value=#{cbUnique} and  ( ( cbi.id=cbr.cb_id and cbr.user_id=#{userId} and cbr.susbcription_status=true  and cbr.is_deleted = false) or cbi.user_id=#{userId} )")
	int validateCookbookSubscriptionUsers(@Param("userId") Long userLong, @Param("cbUnique") String cbUnique);

	@Select("select count(1) from cookbook_reader cbr,cookbook_recipe_map crm  where cbr.cb_id=crm.cb_id and  cbr.user_id=#{userId} and crm.recipe_id=#{recipeId} and cbr.susbcription_status=1 and cbr.is_deleted=0 and crm.is_deleted=0")
	int validateUserCookBookRecipeSubscription(@Param("userId") Long userLong, @Param("recipeId") Long recipeId);
}
