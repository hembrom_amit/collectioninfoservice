package com.reciplay.cis.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

public interface RecipeErrListDAO {

	
	
	@Update("select count(1) from recipe_ingestion_stats where user_id=#{userId} and recipe_id=#{recipeId}")
	int validateIngestedRecipe(@Param("recipeId") long recipeId,@Param("userId") long userId);
	
	
	
}
