package com.reciplay.cis.beans;

public class RecipeStatsVO {

	private long browseCount;
	private boolean newBrowse;
	private long cookCount;
	private boolean newCook;
	private long favoriteCount;
	private boolean newFavorite;
	private long sharedCount;
	private boolean newShared;
	private long commentCount;
	private boolean newComment;
	private long askChefCount;
	private boolean newAskChef;
	
	public RecipeStatsVO() {
	}
	public long getBrowseCount() {
		return browseCount;
	}
	public void setBrowseCount(long browseCount) {
		this.browseCount = browseCount;
	}
	public boolean isNewBrowse() {
		return newBrowse;
	}
	public void setNewBrowse(boolean newBrowse) {
		this.newBrowse = newBrowse;
	}
	public long getCookCount() {
		return cookCount;
	}
	public void setCookCount(long cookCount) {
		this.cookCount = cookCount;
	}
	public boolean isNewCook() {
		return newCook;
	}
	public void setNewCook(boolean newCook) {
		this.newCook = newCook;
	}
	public long getFavoriteCount() {
		return favoriteCount;
	}
	public void setFavoriteCount(long favoriteCount) {
		this.favoriteCount = favoriteCount;
	}
	public boolean isNewFavorite() {
		return newFavorite;
	}
	public void setNewFavorite(boolean newFavorite) {
		this.newFavorite = newFavorite;
	}
	public long getSharedCount() {
		return sharedCount;
	}
	public void setSharedCount(long sharedCount) {
		this.sharedCount = sharedCount;
	}
	public boolean isNewShared() {
		return newShared;
	}
	public void setNewShared(boolean newShared) {
		this.newShared = newShared;
	}
	public long getCommentCount() {
		return commentCount;
	}
	public void setCommentCount(long commentCount) {
		this.commentCount = commentCount;
	}
	public boolean isNewComment() {
		return newComment;
	}
	public void setNewComment(boolean newComment) {
		this.newComment = newComment;
	}
	public long getAskChefCount() {
		return askChefCount;
	}
	public void setAskChefCount(long askChefCount) {
		this.askChefCount = askChefCount;
	}
	public boolean isNewAskChef() {
		return newAskChef;
	}
	public void setNewAskChef(boolean newAskChef) {
		this.newAskChef = newAskChef;
	}
	public RecipeStatsVO(long browseCount, boolean newBrowse, long cookCount, boolean newCook, long favoriteCount,
			boolean newFavorite, long sharedCount, boolean newShared, long commentCount, boolean newComment,
			long askChefCount, boolean newAskChef) {
		this.browseCount = browseCount;
		this.newBrowse = newBrowse;
		this.cookCount = cookCount;
		this.newCook = newCook;
		this.favoriteCount = favoriteCount;
		this.newFavorite = newFavorite;
		this.sharedCount = sharedCount;
		this.newShared = newShared;
		this.commentCount = commentCount;
		this.newComment = newComment;
		this.askChefCount = askChefCount;
		this.newAskChef = newAskChef;
	}
	@Override
	public String toString() {
		return "RecipeStatsVO [browseCount=" + browseCount + ", newBrowse=" + newBrowse + ", cookCount=" + cookCount
				+ ", newCook=" + newCook + ", favoriteCount=" + favoriteCount + ", newFavorite=" + newFavorite
				+ ", sharedCount=" + sharedCount + ", newShared=" + newShared + ", commentCount=" + commentCount
				+ ", newComment=" + newComment + ", askChefCount=" + askChefCount + ", newAskChef=" + newAskChef + "]";
	}
}
