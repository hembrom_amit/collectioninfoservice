package com.reciplay.cis.beans;

/**
 * Overall users statistic data
 * 
 * @author user
 *
 */
public class UserRecipeStatsInfo {

	private long browseCount;
	private long cookCount;
	private long commentCount;
	private long askChefCount;
	private long sharedCount;
	private long favoriteCount;
	private long publishCount;
	private long reviewCount;
	
	public long getBrowseCount() {
		return browseCount;
	}
	public void setBrowseCount(long browseCount) {
		this.browseCount = browseCount;
	}
	public long getCookCount() {
		return cookCount;
	}
	public void setCookCount(long cookCount) {
		this.cookCount = cookCount;
	}
	public long getCommentCount() {
		return commentCount;
	}
	public void setCommentCount(long commentCount) {
		this.commentCount = commentCount;
	}
	public long getAskChefCount() {
		return askChefCount;
	}
	public void setAskChefCount(long askChefCount) {
		this.askChefCount = askChefCount;
	}
	public long getFavoriteCount() {
		return favoriteCount;
	}
	public void setFavoriteCount(long favoriteCount) {
		this.favoriteCount = favoriteCount;
	}
	public long getReviewCount() {
		return reviewCount;
	}
	public void setReviewCount(long reviewCount) {
		this.reviewCount = reviewCount;
	}
	
	public long getSharedCount() {
		return sharedCount;
	}
	public void setSharedCount(long sharedCount) {
		this.sharedCount = sharedCount;
	}
	 
	public long getPublishCount() {
		return publishCount;
	}
	public void setPublishCount(long publishCount) {
		this.publishCount = publishCount;
	}
	public UserRecipeStatsInfo() {
	}
	public UserRecipeStatsInfo( long browseCount, long cookCount, long commentCount, long askChefCount,
			long favoriteCount, long reviewCount) {
		this.browseCount = browseCount;
		this.cookCount = cookCount;
		this.commentCount = commentCount;
		this.askChefCount = askChefCount;
		this.favoriteCount = favoriteCount;
		this.reviewCount = reviewCount;
	}
	@Override
	public String toString() {
		return "RecipeStatsInfo [  browseCount=" + browseCount + ", cookCount=" + cookCount
				+ ", commentCount=" + commentCount + ", askChefCount=" + askChefCount + ", sharedCount=" + sharedCount
				+ ", favoriteCount=" + favoriteCount + ", publishCount=" + publishCount + ", reviewCount=" + reviewCount
				+ "]";
	}
	 
	 
}
