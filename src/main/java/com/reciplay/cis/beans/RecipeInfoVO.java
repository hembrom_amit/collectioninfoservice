package com.reciplay.cis.beans;

public class RecipeInfoVO {

	private long recipeCount;
	private long lastPublishedTime;
	
	public long getRecipeCount() {
		return recipeCount;
	}
	public void setRecipeCount(long recipeCount) {
		this.recipeCount = recipeCount;
	}
	public long getLastPublishedTime() {
		return lastPublishedTime;
	}
	public void setLastPublishedTime(long lastPublishedTime) {
		this.lastPublishedTime = lastPublishedTime;
	}
	@Override
	public String toString() {
		return "RecipeInfoVO [recipeCount=" + recipeCount + ", lastPublishedTime=" + lastPublishedTime + "]";
	}
	public RecipeInfoVO(long recipeCount, long lastPublishedTime) {
		this.recipeCount = recipeCount;
		this.lastPublishedTime = lastPublishedTime;
	}
	public RecipeInfoVO() {
	}
	
	
}
