package com.reciplay.cis.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class KeeperDAOService {

	@Autowired
	SqlSessionTemplate sqlSessionMasterReciplayUser;
	
	
	
	public int validateRecipeInfoForUser(long userId,long recipeId) {
		return sqlSessionMasterReciplayUser.getMapper(KeeperDAO.class).validateRecipeInfoForUser(userId,recipeId);
	}
	
	public int getKitchenAdminRoleCount(long userId,long id) {
		return sqlSessionMasterReciplayUser.getMapper(KeeperDAO.class).getKitchenAdminRoleCount(userId,id);
	}
	
	
	
	
}
