package com.reciplay.cis.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.dao.TransientDataAccessException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import com.reciplay.cis.beans.User;

@Service
public class UserDAOService {

	private static final Logger logger = LoggerFactory.getLogger(UserDAOService.class);

	@Autowired
	SqlSessionTemplate sqlSessionMaster;

	@Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = 3, backoff = @Backoff(delay = 100))
	public User getUserData(long userId) {
		return sqlSessionMaster.getMapper(UserDAO.class).getUserDataByUserId(userId);
	}
	
	

}