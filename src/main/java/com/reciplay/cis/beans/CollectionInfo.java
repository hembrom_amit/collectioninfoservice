package com.reciplay.cis.beans;

public class CollectionInfo {

	private String chefName;
	private int totalRecipeCount;
	private String creationTime;
	public String getChefName() {
		return chefName;
	}
	public void setChefName(String chefName) {
		this.chefName = chefName;
	}
	public int getTotalRecipeCount() {
		return totalRecipeCount;
	}
	public void setTotalRecipeCount(int totalRecipeCount) {
		this.totalRecipeCount = totalRecipeCount;
	}
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}
	public CollectionInfo(String chefName, int totalRecipeCount, String creationTime) {
		this.chefName = chefName;
		this.totalRecipeCount = totalRecipeCount;
		this.creationTime = creationTime;
	}
	
	public CollectionInfo() {
	}
	@Override
	public String toString() {
		return "CollectionInfo [chefName=" + chefName + ", totalRecipeCount=" + totalRecipeCount + ", creationTime="
				+ creationTime + "]";
	}
	
	
}
