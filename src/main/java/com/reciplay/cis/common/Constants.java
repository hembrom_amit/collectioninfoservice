package com.reciplay.cis.common;


public class Constants {
    
    public static class WebConstant {

        public final static String PAYLOAD_NULL_OR_EMPTY ="Payload null or empty"; 
        public final static int PAYLOAD_NULL_OR_EMPTY_ERRCODE=18;
        public final static String INVALID_JSON ="Invalid JSON"; 
        public final static int INVALID_JSON_ERRCODE=19;
        public final static String IMPROPER_PAYLOAD ="Improper payload"; 
        public final static int IMPROPER_PAYLOAD_ERRCODE=20;
        public final static String INVALID_USERID ="Invalid UserId"; 
        public final static int INVALID_USERID_ERRCODE=101;
        public final static String INVALID_RECIPEID ="Invalid RecipeId"; 
        public final static int INVALID_RECIPEID_ERRCODE=102;
        public final static String INVALID_PAGEID ="Invalid PageId"; 
        public final static int INVALID_PAGEID_ERRCODE=103;
        public final static String INVALID_SORTBY ="Invalid Sort by.Please pass value between 0 and 5"; 
        public final static int INVALID_SORTBY_ERRCODE=104;
        public final static String INVALID_COOKBOOKID ="Invalid Cookbook id."; 
        public final static int INVALID_COOKBOOKID_ERRCODE=105;
        public final static String INVALID_RECEIPEPERPAGE ="Invalid Receipe per page count."; 
        public final static int INVALID_RECEIPEPERPAGE_ERRCODE=106;
        
        public final static String ERROR_MESSAGE = "ErrorMessage";
        public final static String ERROR_CODE = "ErrorCode";
        
        
        public final static String UNABLE_TO_PROCESS_REQUEST ="Unable to process request"; 
        public final static int UNABLE_TO_PROCESS_REQUEST_ERRCODE=100;
        
        
        public final static String AUTHENTICATION_DATA="authenticationData";
        public final static String USERID="userid";
    }
    
    public static class StatsConstant{
    	public final static int DEFAULT_SORT=0;
    	public final static int SORT_BY_CURRENT_MONTH_COUNT=1;
    	public final static int SORT_BY_PREVIOUS_MONTH_COUNT=2;
    	public final static int SORT_BY_TOTAL_COUNT=3;
    	public final static int SORT_BY_NAME=4;
    	public final static int SORT_BY_PUBLISHED_DATE=5;
    }
    
}
