package com.reciplay.cis.service;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reciplay.cis.beans.CollectionBook;
import com.reciplay.cis.beans.RecipeData;
import com.reciplay.cis.dao.CollectionListDAOService;

@Service
public class RecipeListService {

	private static final Logger logger = LoggerFactory.getLogger(RecipeListService.class);

	@Autowired
	CollectionListDAOService collectionDAO;
	
	public JSONArray fetchAndReturnCollectionCB(long userId) {
		JSONArray jsonObj = null;
		List<CollectionBook> collectionBookList = collectionDAO.getCollected(userId);
		System.out.println("CollectionBookList Service "+collectionBookList.toString());
		if(collectionBookList!=null && collectionBookList.size()>0) {
			String allRecipeIds = getAllRecipeIds(collectionBookList);
			Map<Long,RecipeData> recipyNameById = collectionDAO.getRecipeName(allRecipeIds);
			for(CollectionBook cb : collectionBookList) {
				if(recipyNameById.containsKey(cb.getRecipeId())) {
					cb.setAuthorName(recipyNameById.get(cb.getRecipeId()).getChefName());
					cb.setRecipeName(recipyNameById.get(cb.getRecipeId()).getRecipeName());
				}
			}
			jsonObj = new JSONArray(collectionBookList);
		}
		
		return jsonObj;
	}

	private String getAllRecipeIds(List<CollectionBook> collectionBookList) {
		StringBuilder allRecipeIds =new StringBuilder();
		for(int i=0;i<collectionBookList.size();i++) {
			if(allRecipeIds.length()>0) {
				allRecipeIds.append(","+collectionBookList.get(i).getRecipeId());
			} else {
				allRecipeIds.append(collectionBookList.get(i).getRecipeId());
			}
		}
		return allRecipeIds.toString();
	}
}
