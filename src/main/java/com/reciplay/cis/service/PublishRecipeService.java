package com.reciplay.cis.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.reciplay.cis.beans.PayloadInfo;
import com.reciplay.cis.dao.RecipeActionDAOService;

@Service
public class PublishRecipeService {

	private static final Logger logger = LoggerFactory.getLogger(PublishRecipeService.class);
	
	@Autowired
	RecipeActionDAOService recipeActionDAOService;
	
	@Transactional("transactionManagerMaster")
	public void publishAction(PayloadInfo payloadInfo) {
		try {
			logger.info("Starting action publishAction.");
			// Insert in action_publish table
			recipeActionDAOService.insertActionPublishTable(payloadInfo.getUserId(), payloadInfo.getRecipeId());
			logger.info("publishAction Done.");
			// Insert in user_recipe_published_info 
			recipeActionDAOService.userPublishRecipedData(payloadInfo.getUserId(), payloadInfo.getRecipeId());
			// Insert or update recipe_stats_table table and update the publish count
			recipeActionDAOService.insertUpdateRecipeStatsTablePublish(payloadInfo.getUserId(),
					payloadInfo.getRecipeId());
			logger.info("insertUpdateRecipeStatsTablePublish Done.");

			 
		} catch (Exception e) {
			System.out.println("Err == " + e.getCause());
			logger.error("Error browseAction ", e.getMessage());
			throw e;
		}

	}
}
