package com.reciplay.cis.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.dao.TransientDataAccessException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import com.reciplay.cis.beans.IntermStatsData;
import com.reciplay.cis.beans.RecipeInfo;
import com.reciplay.cis.beans.RecipeInfoVO;
import com.reciplay.cis.beans.RecipeMonthStats;
import com.reciplay.cis.beans.RecipeStatsVO;
import com.reciplay.cis.beans.UserRecipeStatsInfo;
import com.reciplay.cis.common.Constants.StatsConstant;

@Service
public class RecipeActionDAOService {

	
	@Autowired
	SqlSessionTemplate sqlSessionMasterReciplayUser;
	
	@Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = 3, backoff = @Backoff(delay = 100))
	public void insertActionBrowseTable(long userId , long recipeId) {
		sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).insertActionBrowseTable(userId, recipeId , System.currentTimeMillis());
	}
	
	@Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = 3, backoff = @Backoff(delay = 100))
	public void updateRecipeStatsTableBrowse(  long recipeId) {
		sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).updateRecipeStatsTableBrowse( recipeId , System.currentTimeMillis());
	}

	
	@Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = 3, backoff = @Backoff(delay = 100))
	public Boolean ifPublishedRecipe(long recipeId) {
		int count = sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).checkIfPublishedRecipe(recipeId);
		if(count >0) {
			return true;
		}
		return false;
	}


	public long getBrowseCount(long userId) {
		return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getBrowseCount(userId);
	}


	public void updateIntermStatsData(long userId,String currDataUpdated) {
		sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).updateIntermStatsData(userId,currDataUpdated);
	}

	@Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = 3, backoff = @Backoff(delay = 100))
	public boolean checkIfFavorite(long userId, long recipeId) {
		if(sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).checkIfFavorite(userId,recipeId)>0) {
			return true;
		}
		return false;
	}

	@Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = 3, backoff = @Backoff(delay = 100))
	public long getUserBrowseId(long userId, long recipeId) {
		return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getUserBrowseId(userId,recipeId);
	}

	@Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = 3, backoff = @Backoff(delay = 100))
	public void updateUserFavoriteRecipeInfo(long userId, long recipeId, long browseId) {
		sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).updateUserFavoriteRecipeInfo(userId,recipeId,browseId,System.currentTimeMillis());
		
	}
	@Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = 3, backoff = @Backoff(delay = 100))
	public RecipeInfoVO getTotalPublishedRecipeCount(long userId) {
		return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).totalPublishedRecipeCount(userId);
	}
	@Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = 3, backoff = @Backoff(delay = 100))
	public void insertActionPublishTable(long userId, long recipeId) {
		sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).insertActionPublishTable(userId, recipeId , System.currentTimeMillis());
	}
	@Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = 3, backoff = @Backoff(delay = 100))
	public void insertUpdateRecipeStatsTablePublish(long userId, long recipeId) {
		sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).insertUpdateRecipeStatsTablePublish(userId, recipeId , System.currentTimeMillis());
	}

	public List <RecipeInfo> getAllPublishedRecipes(long userId, int pageid) {
		 
		return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getAllPublishedRecipesByid(userId,pageid*6,7);
	}

	public void userPublishRecipedData(long userId, long recipeId) {
		sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).userPublishRecipedData(userId,recipeId,System.currentTimeMillis());
		
	}
	
	public List <RecipeMonthStats> getAllPublishedRecipesByPublishedDate(String recipeIdStr, int pageid,int recipePerPage ) {
		return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getAllPublishedRecipesByPublishedDate(recipeIdStr,(pageid*recipePerPage),(recipePerPage+1));
	}
	
	public List <RecipeMonthStats> getAllPublishedRecipesByPublishedDateCook(String recipeIdStr, int pageid ,int recipePerPage) {
		return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getAllPublishedRecipesByPublishedDateCook(recipeIdStr,(pageid*recipePerPage),(recipePerPage+1));
	}

	public int getTotalBrowseCount(long userId) {
		return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getTotalBrowseCount(userId);
	}

	public List<Long> getAllPublishedRecipeIds(long userId) {
		return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getAllPublishedRecipeIds(userId);
	}

	public UserRecipeStatsInfo getTotalBrowseCountByRecipeIds(String allRecipeStr) {
		return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getStatsDataForUser(allRecipeStr);
	}

/*	public void insertRecipeStatsMonthly(long recipeId ,long count ) {
		 sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).insertRecipeStatsMonthly(recipeId,count,System.currentTimeMillis());
		
	}

	public void incrementBrowseCountMonthlyRecipeStats( long recipeId) {

		sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).incrementBrowseCountMonthlyRecipeStats( recipeId,System.currentTimeMillis());
	}*/

	public boolean checkRecipeHistory(int month, int year, long recipeId) {
		if( sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).checkRecipeHistory(month,year,recipeId) >0 ) {
			return true;
		}
		return false;
	}

	public void insertIntoRecipeHistory(int month, int year, long recipeId) {
		sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).insertIntoRecipeHistory(month,year,recipeId);
	}

	public long getChefIdForRecipe(long recipeId) {
		return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getChefIdForRecipe(recipeId);
	}

	public void insertIntermStatsData(long chefId, IntermStatsData internStatsData) {
		 sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).insertIntermStatsData(chefId,internStatsData);
	}

	public UserRecipeStatsInfo getStatsDataForUser(String allRecipeStr) {
		return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getStatsDataForUser(allRecipeStr);
	}

	public int browseCountForMonth(long recipeId, int month, int year) {
		Integer browseCount = sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).browseCountForMonth(recipeId,month,year);
		if(browseCount==null) {
			return 0;
		}
		return  browseCount.intValue();
	}

	public void updateRecipeStatsTableBrowseCurrentMonthData(long recipeId, int countPreviousMonth) {
		  sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).updateRecipeStatsTableBrowseCurrentMonthData(recipeId,countPreviousMonth,System.currentTimeMillis());
		
	}

	public IntermStatsData getUserInterimStatsData(long userId) {
		return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getUserInterimStatsData(userId);
	}

	public List<RecipeMonthStats> getRecipeStatsBrowsedByRecipeStr(String allRecipeStr,int sortBy , int page , int recipePerPage) {
		if(sortBy == StatsConstant.DEFAULT_SORT || sortBy ==StatsConstant.SORT_BY_CURRENT_MONTH_COUNT ) {
			return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getRecipeStatsBrowsedByRecipeStrSortByCurrent(allRecipeStr,(page*recipePerPage), (recipePerPage+1));
		} else if( sortBy ==StatsConstant.SORT_BY_PREVIOUS_MONTH_COUNT ) {
			return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getRecipeStatsBrowsedByRecipeStrSortByPrevious(allRecipeStr,(page*recipePerPage), (recipePerPage+1));
		} else if( sortBy ==StatsConstant.SORT_BY_TOTAL_COUNT ) {
			return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getRecipeStatsBrowsedByRecipeStrSortByTot(allRecipeStr,(page*recipePerPage), (recipePerPage+1));
		} else if( sortBy ==StatsConstant.SORT_BY_NAME ) {
			return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getRecipeStatsBrowsedByRecipeStrSortByName(allRecipeStr,(page*recipePerPage), (recipePerPage+1));
		}  
		return null;
	}
	
	public List<RecipeMonthStats> getRecipeStatsCookByRecipeStr(String allRecipeStr,int sortBy , int page,int recipePerPage) {
		if(sortBy == StatsConstant.DEFAULT_SORT || sortBy ==StatsConstant.SORT_BY_CURRENT_MONTH_COUNT ) {
			return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getRecipeStatsCookByRecipeStrSortByCurrent(allRecipeStr,(page*recipePerPage), (recipePerPage+1));
		} else if( sortBy ==StatsConstant.SORT_BY_PREVIOUS_MONTH_COUNT ) {
			return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getRecipeStatsCookByRecipeStrSortByPrevious(allRecipeStr,(page*recipePerPage), (recipePerPage+1));
		} else if( sortBy ==StatsConstant.SORT_BY_TOTAL_COUNT ) {
			return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getRecipeStatsCookByRecipeStrSortByTot(allRecipeStr,(page*recipePerPage), (recipePerPage+1));
		} else if( sortBy ==StatsConstant.SORT_BY_NAME ) {
			return sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).getRecipeStatsCookByRecipeStrSortByName(allRecipeStr,(page*recipePerPage), (recipePerPage+1));
		}  
		return null;
	}

	public void insertActionCookTable(long userId, long recipeId) {
		sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).insertActionCookTable(userId,recipeId,System.currentTimeMillis());
	}

	public void updateRecipeStatsTableCook(long recipeId) {
		sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).updateRecipeStatsTableCook( recipeId,System.currentTimeMillis() );
		
	}

	public int cookCountForMonth(long recipeId, int month, int year) {
		Integer cookCount = sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).cookCountForMonth(recipeId,month,year);
		if(cookCount==null) {
			return 0;
		}
		return  cookCount.intValue();
	}

	public void updateRecipeStatsTableCookPreviousMonthData(long recipeId, int countPreviousMonth) {
		sqlSessionMasterReciplayUser.getMapper(RecipeActionDAO.class).updateRecipeStatsTableCookPreviousMonthData(recipeId,countPreviousMonth,System.currentTimeMillis());
		
	}

}
