package com.reciplay.cis.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.reciplay.cis.beans.CollectionBook;
import com.reciplay.cis.beans.RecipeData;

public interface CollectionListDAO {
  
	@Select("SELECT user_id as userId, recipe_id as recipeId, timestamp as lastVisitTime FROM action_collect WHERE user_id = #{userId}")
	List<CollectionBook> getAllCollections(@Param("userId") long userId);

	@Select("SELECT rd.recipe_id as recipeId, rd.cook_user_id as chefId, rd.recipe_name as recipeName, u.firstname as chefName FROM recipe_data rd , users u WHERE rd.cook_user_id=u.id and rd.recipe_id in ( ${allRecipeIds})")
	List<RecipeData> getRecipeData(@Param("allRecipeIds")String allRecipeIds);

	
}