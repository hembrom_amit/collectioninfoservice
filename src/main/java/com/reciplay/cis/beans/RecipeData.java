package com.reciplay.cis.beans;

public class RecipeData {

	private long recipeId;
	private long chefId;
	private String recipeName;
	private String chefName;
	
	public long getRecipeId() {
		return recipeId;
	}
	public void setRecipeId(long recipeId) {
		this.recipeId = recipeId;
	}
	public long getChefId() {
		return chefId;
	}
	public void setChefId(long chefId) {
		this.chefId = chefId;
	}
	public String getRecipeName() {
		return recipeName;
	}
	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}
	public RecipeData(long recipeId, long chefId, String recipeName) {
		this.recipeId = recipeId;
		this.chefId = chefId;
		this.recipeName = recipeName;
	}
	
	public String getChefName() {
		return chefName;
	}
	public void setChefName(String chefName) {
		this.chefName = chefName;
	}
	public RecipeData() {
	}
	@Override
	public String toString() {
		return "RecipeData [recipeId=" + recipeId + ", chefId=" + chefId + ", recipeName=" + recipeName + ", chefName="
				+ chefName + "]";
	}
}
