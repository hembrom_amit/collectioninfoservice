package com.reciplay.cis.beans;

public class RecipeInfo {

	private long recipeId;
	private String recipeImage;
	private String recipeName;
	private String metaData;
	private String reviewRating;
	private int reviewCount;
	private String reipeInfo;
	private long browseCount;
	private long cookCount;
	private long commentCount;
	private long askChefCount;
	private long sharedCount;
	private long favoriteCount;
	private long publishCount;
	private int recipeStar;
	private long timestamp;
	private long publishedTime;
	
	
	
	
	public int getRecipeStar() {
		return recipeStar;
	}
	public void setRecipeStar(int recipeStar) {
		this.recipeStar = recipeStar;
	}
	public long getRecipeId() {
		return recipeId;
	}
	public void setRecipeId(long recipeId) {
		this.recipeId = recipeId;
	}
	public String getRecipeImage() {
		return recipeImage;
	}
	public void setRecipeImage(String recipeImage) {
		this.recipeImage = recipeImage;
	}
	public String getRecipeName() {
		return recipeName;
	}
	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}
	public String getMetaData() {
		return metaData;
	}
	public void setMetaData(String metaData) {
		this.metaData = metaData;
	}
	public String getReviewRating() {
		return reviewRating;
	}
	public void setReviewRating(String reviewRating) {
		this.reviewRating = reviewRating;
	}
	public int getReviewCount() {
		return reviewCount;
	}
	public void setReviewCount(int reviewCount) {
		this.reviewCount = reviewCount;
	}
	public String getReipeInfo() {
		return reipeInfo;
	}
	public void setReipeInfo(String reipeInfo) {
		this.reipeInfo = reipeInfo;
	}
	
	public long getBrowseCount() {
		return browseCount;
	}
	public void setBrowseCount(long browseCount) {
		this.browseCount = browseCount;
	}
	public long getCookCount() {
		return cookCount;
	}
	public void setCookCount(long cookCount) {
		this.cookCount = cookCount;
	}
	public long getCommentCount() {
		return commentCount;
	}
	public void setCommentCount(long commentCount) {
		this.commentCount = commentCount;
	}
	public long getAskChefCount() {
		return askChefCount;
	}
	public void setAskChefCount(long askChefCount) {
		this.askChefCount = askChefCount;
	}
	public long getSharedCount() {
		return sharedCount;
	}
	public void setSharedCount(long sharedCount) {
		this.sharedCount = sharedCount;
	}
	public long getFavoriteCount() {
		return favoriteCount;
	}
	public void setFavoriteCount(long favoriteCount) {
		this.favoriteCount = favoriteCount;
	}
	public long getPublishCount() {
		return publishCount;
	}
	public void setPublishCount(long publishCount) {
		this.publishCount = publishCount;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public long getPublishedTime() {
		return publishedTime;
	}
	public void setPublishedTime(long publishedTime) {
		this.publishedTime = publishedTime;
	}
	public RecipeInfo(long recipeId, String recipeImage, String recipeName, String metaData, String reviewRating,
			int reviewCount, String reipeInfo) {
		this.recipeId = recipeId;
		this.recipeImage = recipeImage;
		this.recipeName = recipeName;
		this.metaData = metaData;
		this.reviewRating = reviewRating;
		this.reviewCount = reviewCount;
		this.reipeInfo = reipeInfo;
	}
	public RecipeInfo() {
	}
	@Override
	public String toString() {
		return "RecipeInfo [recipeId=" + recipeId + ", recipeImage=" + recipeImage + ", recipeName=" + recipeName
				+ ", metaData=" + metaData + ", reviewRating=" + reviewRating + ", reviewCount=" + reviewCount
				+ ", reipeInfo=" + reipeInfo + ", browseCount=" + browseCount + ", cookCount=" + cookCount
				+ ", commentCount=" + commentCount + ", askChefCount=" + askChefCount + ", sharedCount=" + sharedCount
				+ ", favoriteCount=" + favoriteCount + ", publishCount=" + publishCount + ", recipeStar=" + recipeStar
				+ ", timestamp=" + timestamp + ", publishedTime=" + publishedTime + "]";
	}
}
