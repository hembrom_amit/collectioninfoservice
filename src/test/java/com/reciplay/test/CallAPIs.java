package com.reciplay.test;
 
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class CallAPIs {

	public static void main(String[] args) {

		for(int i=0;i<1;i++){
			try {
				postAPI();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void postAPI() throws UnsupportedEncodingException{
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost("http://localhost:8080/collectioninfoservice/restAPI/recipeaction/publish/");
		//HttpPost post = new HttpPost("http://ec2-13-126-114-42.ap-south-1.compute.amazonaws.com:8080/collectioninfoservice/restAPI/recipeaction/publish/");
		
		for(int i=104;i<145;i++) {
			// Create some NameValuePair for HttpPost parameters
	        StringEntity params =new StringEntity("{\"recipeId\":"+i+" , \"userId\":22,\"cookbookId\":1}");
	        post.setHeader("Accept", "application/json");
	        post.setHeader("Content-type", "application/json");
	        try {
	            post.setEntity(params);
	            HttpResponse response = client.execute(post);
	            System.out.println("Done for recipe id "+i);
	            // Print out the response message
	            System.out.println(EntityUtils.toString(response.getEntity()));
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		}
        
	}

}
