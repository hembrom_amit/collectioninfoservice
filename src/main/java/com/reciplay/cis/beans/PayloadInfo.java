package com.reciplay.cis.beans;

public class PayloadInfo {

	private long userId;
	private long recipeId;
	private long cookbookId;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getRecipeId() {
		return recipeId;
	}
	public void setRecipeId(long recipeId) {
		this.recipeId = recipeId;
	}
	public PayloadInfo(long userId, long recipeId) {
		this.userId = userId;
		this.recipeId = recipeId;
	}
	
	public long getCookbookId() {
		return cookbookId;
	}
	public void setCookbookId(long cookbookId) {
		this.cookbookId = cookbookId;
	}
	public PayloadInfo() {
	}
	@Override
	public String toString() {
		return "PayloadInfo [userId=" + userId + ", recipeId=" + recipeId + ", cookbookId=" + cookbookId + "]";
	}
}
