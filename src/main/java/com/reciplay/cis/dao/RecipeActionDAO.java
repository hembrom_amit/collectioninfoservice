package com.reciplay.cis.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.reciplay.cis.beans.IntermStatsData;
import com.reciplay.cis.beans.RecipeInfo;
import com.reciplay.cis.beans.RecipeInfoVO;
import com.reciplay.cis.beans.RecipeMonthStats;
import com.reciplay.cis.beans.UserRecipeStatsInfo;

public interface RecipeActionDAO {

	@Insert("INSERT INTO action_browse(user_id, recipe_id,timestamp) "
			+ "VALUES(#{userId},#{recipeId},#{currentTimeMS}) ")
	void insertActionBrowseTable(@Param("userId") long userId,@Param("recipeId") long recipeId,@Param("currentTimeMS") long currentTimeMS);
	
	@Update("UPDATE recipe_stats_info set browse_count_total=browse_count_total+1 ,browsed_current_month=browsed_current_month+1,"
			+ " last_updated=#{currentTimeMS} where recipe_id=#{recipeId}")
	void updateRecipeStatsTableBrowse( @Param("recipeId") long recipeId,@Param("currentTimeMS") long currentTimeMS);

	//@Select("select recipe_publish_status from recipe_data where recipe_id = #{recipeId}")
	@Select("select count(1) from user_recipe_info where recipe_id = #{recipeId} and "
			+ "is_published=1 and is_deleted=0")
	int checkIfPublishedRecipe(@Param("recipeId") long recipeId);
 
	@Select("select count(1) from action_browse where user_id = #{userId}")
	long getBrowseCount(@Param("userId") long userId);

	@Insert("INSERT INTO user_interim_stats(user_id, currentStats) VALUES(#{userId},#{currDataUpdated}) "
			+ "ON DUPLICATE KEY UPDATE  currentStats = #{currDataUpdated}")
	void updateIntermStatsData(@Param("userId") long userId , @Param("currDataUpdated") String currDataUpdated);

	@Select("select count(1) from user_favorite_recipe_info where user_id = #{userId} and recipe_id=#{recipeId}")
	long checkIfFavorite(@Param("userId") long userId,@Param("recipeId") long recipeId);

	@Select("select id from action_browse where user_id = #{userId} and recipe_id=#{recipeId} desc limit 1")
	long getUserBrowseId(@Param("userId") long userId,@Param("recipeId") long recipeId);

	@Insert("INSERT INTO user_favorite_recipe_info(user_id, recipe_id, browsed_id,last_updated) "
			+ "VALUES(#{userId},#{recipeId},#{browseId},#{currentTimeMS}) ON DUPLICATE KEY UPDATE recipe_id=#{recipeId} , last_updated=#{currentTimeMS}")
	void updateUserFavoriteRecipeInfo(@Param("userId") long userId,@Param("recipeId") long recipeId,@Param("browseId") long browseId,@Param("currentTimeMS")long currentTimeMS);

	@Select("select count(distinct(recipe_id)) as recipeCount , max(publish_date) as lastPublishedTime from user_recipe_info where user_id=#{userId} and is_published=1 and is_deleted=0")
	RecipeInfoVO totalPublishedRecipeCount(@Param("userId")  long userId);

	@Insert("INSERT INTO action_publish(user_id, recipe_id,timestamp) "
			+ "VALUES(#{userId},#{recipeId},#{currentTimeMS}) ")
	void insertActionPublishTable(@Param("userId") long userId,@Param("recipeId") long recipeId,@Param("currentTimeMS") long currentTimeMS);
	
	@Insert("INSERT INTO recipe_stats_info( recipe_id, publish_count_total,last_updated) "
			+ "VALUES(#{recipeId},1,#{currentTimeMS}) ON DUPLICATE KEY UPDATE publish_count_total=publish_count_total+1 , last_updated=#{currentTimeMS}")
	void insertUpdateRecipeStatsTablePublish(@Param("userId") long userId,@Param("recipeId") long recipeId,@Param("currentTimeMS") long currentTimeMS);

	@Select("select distinct(recipe_id) as recipeId , publish_date as publishedTime from user_recipe_info where user_id = #{userId}")
	List <RecipeInfo> getAllPublishedRecipesByid(@Param("userId")long userId,@Param("offset")int offset , @Param("limit")int limit);
	
	@Select("select  pub.recipe_id as recipeId , rst.browsed_current_month as currentMonthCount ,rst.browsed_previous_month as previousMonthCount , "
			+ "rst.browse_count_total as total , pub.recipe_name as recipeName from recipe_stats_info rst ,  user_recipe_info pub "
			+ "where  rst.recipe_id= pub.recipe_id and "
			+ "pub.recipe_id in ( ${allRecipeStr}) order by pub.publish_date desc LIMIT #{offset},#{limit}")
	List <RecipeMonthStats> getAllPublishedRecipesByPublishedDate (@Param("allRecipeStr")String allRecipeStr,@Param("offset")int offset , @Param("limit")int limit);
	 
	
	@Select("select  pub.recipe_id as recipeId , rst.cooked_current_month as currentMonthCount ,rst.cooked_previous_month as previousMonthCount , "
			+ "rst.cooked_count_total as total , pub.recipe_name as recipeName from recipe_stats_info rst ,  user_recipe_info pub "
			+ "where  rst.recipe_id= pub.recipe_id and "
			+ "pub.recipe_id in ( ${allRecipeStr}) order by pub.publish_date desc LIMIT #{offset},#{limit}")
	List <RecipeMonthStats> getAllPublishedRecipesByPublishedDateCook (@Param("allRecipeStr")String allRecipeStr,@Param("offset")int offset , @Param("limit")int limit);
	 
	
	@Select("select browse_count from recipe_stats_info where user_id = #{userId}")
	int getTotalBrowseCount(@Param("userId") long userId);

	@Insert("INSERT INTO user_recipe_info(user_id, recipe_id,publish_date,is_published,last_updated) "
			+ "VALUES(#{userId},#{recipeId},#{currentTimeMS},1,#{currentTimeMS} ) ON DUPLICATE KEY UPDATE is_published=1 ,last_updated=#{currentTimeMS}")
	void userPublishRecipedData(@Param("userId")long userId,@Param("recipeId")  long recipeId ,@Param("currentTimeMS") long currentTimeMS);

	@Select("select recipe_id from user_recipe_info where user_id=#{userId} and is_published=1 and is_deleted=0")
	List<Long> getAllPublishedRecipeIds(@Param("userId")long userId);

	@Select("select sum(browse_count) from recipe_stats_info  where recipe_id in (${allRecipeStr})")
	int getTotalBrowseCountByRecipeIds(@Param("allRecipeStr") String allRecipeStr);
	@Select("select count(1) from recipe_stats_history  where recipe_id = #{recipeId} and "
			+ "month=#{month} and year=#{year}")
	int checkRecipeHistory(@Param("month")int month, @Param("year") int year, @Param("recipeId")long recipeId);
	
	@Insert("INSERT INTO recipe_stats_history(recipe_id, month,year)  VALUES(#{recipeId},#{month},#{year}) ")
	void insertIntoRecipeHistory(@Param("month")int month, @Param("year") int year, @Param("recipeId") long recipeId);
	
	@Select("select user_id from user_recipe_info  where recipe_id = #{recipeId} ")
	long getChefIdForRecipe(@Param("recipeId") long recipeId);

	@Insert("INSERT INTO user_interim_stats(user_id, currentStats,previousStats)  VALUES(#{chefId},#{internStatsData.runningStatsData},#{internStatsData.previousStatsData}) ")
	void insertIntermStatsData(@Param("chefId")  long chefId, @Param("internStatsData") IntermStatsData internStatsData);

	@Select("select   sum(browse_count_total) as browseCount , sum(cooked_count_total) as cookCount ,sum(review_count_total) as reviewCount ," 
			+ "sum(comment_count_total) as commentCount , sum(ask_chef_count_total) as askChefCount ," 
			+ "sum(shared_count_total) as sharedCount ,sum(favorite_count_total) as favoriteCount from recipe_stats_info where"
			+ " recipe_id in (${allRecipeStr}) ")
	UserRecipeStatsInfo getStatsDataForUser(@Param("allRecipeStr") String allRecipeStr);

	@Select("select browsed_count from recipe_stats_history  where recipe_id = #{recipeId} and month=#{month} and year=#{year} ")
	Integer browseCountForMonth(@Param("recipeId")long recipeId,@Param("month") int month,@Param("year") int year);

	@Update("UPDATE recipe_stats_info set browse_count_total=browse_count_total+1 ,browsed_current_month=browsed_current_month+1,"
			+ " browsed_previous_month =#{countPreviousMonth} , last_updated=#{currentTimeMS} where recipe_id=#{recipeId}")
	void updateRecipeStatsTableBrowseCurrentMonthData(@Param("recipeId") long recipeId, @Param("countPreviousMonth") int countPreviousMonth,
			@Param("currentTimeMS") long currentTimeMillis );

	@Select("select user_id as userId , currentStats as runningStatsData , "
			+ "previousStats as previousStatsData , last_view_time as lastStatsViewTime "
			+ " from user_interim_stats  where user_id=#{userId}")
	IntermStatsData getUserInterimStatsData(@Param("userId")long userId);

	@Select("select rsi.recipe_id as recipeId ,"
			+ "rsi.browsed_current_month as currentMonthCount ,rsi.browsed_previous_month as previousMonthCount ,"
			+ "rsi.browse_count_total as total , uri.recipe_name as recipeName "
			+ "from  recipe_stats_info  rsi , user_recipe_info uri where rsi.recipe_id in (${allRecipeStr})"
			+ " and rsi.recipe_id=uri.recipe_id order by currentMonthCount desc LIMIT #{offset},#{limit} ")
	List<RecipeMonthStats> getRecipeStatsBrowsedByRecipeStrSortByCurrent(@Param("allRecipeStr") String allRecipeStr, @Param("offset")int offset , @Param("limit")int limit);
	
	@Select("select rsi.recipe_id as recipeId ,"
			+ "rsi.browsed_current_month as currentMonthCount ,rsi.browsed_previous_month as previousMonthCount ,"
			+ "rsi.browse_count_total as total , uri.recipe_name as recipeName "
			+ "from  recipe_stats_info  rsi , user_recipe_info uri where rsi.recipe_id in (${allRecipeStr}) "
			+ " and rsi.recipe_id=uri.recipe_id  order by previousMonthCount desc LIMIT #{offset},#{limit} ")
	List<RecipeMonthStats> getRecipeStatsBrowsedByRecipeStrSortByPrevious(@Param("allRecipeStr") String allRecipeStr, @Param("offset")int offset , @Param("limit")int limit);
	
	@Select("select rsi.recipe_id as recipeId ,"
			+ "rsi.browsed_current_month as currentMonthCount ,rsi.browsed_previous_month as previousMonthCount ,"
			+ "rsi.browse_count_total as total , uri.recipe_name as recipeName "
			+ "from  recipe_stats_info  rsi , user_recipe_info uri   where rsi.recipe_id in (${allRecipeStr})"
			+ " and rsi.recipe_id=uri.recipe_id   order by total desc LIMIT #{offset},#{limit} ")
	List<RecipeMonthStats> getRecipeStatsBrowsedByRecipeStrSortByTot(@Param("allRecipeStr") String allRecipeStr, @Param("offset")int offset , @Param("limit")int limit);
	
	@Select("select rsi.recipe_id as recipeId ,"
			+ "rsi.browsed_current_month as currentMonthCount ,rsi.browsed_previous_month as previousMonthCount ,"
			+ "rsi.browse_count_total as total , uri.recipe_name as recipeName "
			+ "from  recipe_stats_info  rsi , user_recipe_info uri  where rsi.recipe_id in (${allRecipeStr})"
			+ " and rsi.recipe_id=uri.recipe_id  order by recipeName asc LIMIT #{offset},#{limit} ")
	List<RecipeMonthStats> getRecipeStatsBrowsedByRecipeStrSortByName(@Param("allRecipeStr") String allRecipeStr, @Param("offset")int offset , @Param("limit")int limit);

	// COOK //
	@Select("select rsi.recipe_id as recipeId ,"
			+ "rsi.cooked_current_month as currentMonthCount ,rsi.cooked_previous_month as previousMonthCount ,"
			+ "rsi.cooked_count_total as total , uri.recipe_name as recipeName "
			+ "from   recipe_stats_info  rsi , user_recipe_info uri  where rsi.recipe_id in (${allRecipeStr}) " + 
			" and rsi.recipe_id=uri.recipe_id  order by currentMonthCount desc LIMIT #{offset},#{limit} ")
	List<RecipeMonthStats> getRecipeStatsCookByRecipeStrSortByCurrent(@Param("allRecipeStr") String allRecipeStr, @Param("offset")int offset , @Param("limit")int limit);
	
	@Select("select rsi.recipe_id as recipeId ,"
			+ "rsi.cooked_current_month as currentMonthCount ,rsi.cooked_previous_month as previousMonthCount ,"
			+ "rsi.cooked_count_total as total , uri.recipe_name as recipeName "
			+ "from   recipe_stats_info  rsi , user_recipe_info uri  where rsi.recipe_id in (${allRecipeStr}) " + 
			" and rsi.recipe_id=uri.recipe_id order by previousMonthCount desc LIMIT #{offset},#{limit} ")
	List<RecipeMonthStats> getRecipeStatsCookByRecipeStrSortByPrevious(@Param("allRecipeStr") String allRecipeStr, @Param("offset")int offset , @Param("limit")int limit);
	
	@Select("select rsi.recipe_id as recipeId ,"
			+ "rsi.cooked_current_month as currentMonthCount ,rsi.cooked_previous_month as previousMonthCount ,"
			+ "rsi.cooked_count_total as total , uri.recipe_name as recipeName "
			+ "from   recipe_stats_info  rsi , user_recipe_info uri  where rsi.recipe_id in (${allRecipeStr}) " + 
			" and rsi.recipe_id=uri.recipe_id order by total desc LIMIT #{offset},#{limit} ")
	List<RecipeMonthStats> getRecipeStatsCookByRecipeStrSortByTot(@Param("allRecipeStr") String allRecipeStr, @Param("offset")int offset , @Param("limit")int limit);
	
	@Select("select rsi.recipe_id as recipeId ,"
			+ "rsi.cooked_current_month as currentMonthCount ,rsi.cooked_previous_month as previousMonthCount ,"
			+ "rsi.cooked_count_total as total , uri.recipe_name as recipeName "
			+ "from   recipe_stats_info  rsi , user_recipe_info uri  where rsi.recipe_id in (${allRecipeStr}) " + 
			" and rsi.recipe_id=uri.recipe_id order by recipeName asc LIMIT #{offset},#{limit} ")
	List<RecipeMonthStats> getRecipeStatsCookByRecipeStrSortByName(@Param("allRecipeStr") String allRecipeStr, @Param("offset")int offset , @Param("limit")int limit);

	
	
	@Insert("INSERT INTO action_cook(user_id, recipe_id,timestamp) "
			+ "VALUES(#{userId},#{recipeId},#{currentTimeMS}) ")
	void insertActionCookTable(@Param("userId") long userId,@Param("recipeId") long recipeId,@Param("currentTimeMS") long currentTimeMS);

	@Update("UPDATE recipe_stats_info set cooked_count_total=cooked_count_total+1 ,cooked_current_month=cooked_current_month+1,"
			+ " last_updated=#{currentTimeMS} where recipe_id=#{recipeId}")
	void updateRecipeStatsTableCook(@Param("recipeId") long recipeId,@Param("currentTimeMS") long currentTimeMillis);
	
	@Update("UPDATE recipe_stats_info set ask_chef_count_total=ask_chef_count_total+1 ,askchef_current_month=askchef_current_month+1,"
			+ " last_updated=#{currentTimeMS} where recipe_id=#{recipeId}")
	void updateRecipeStatsTableAskchef(@Param("recipeId") long recipeId);
	
	
	@Update("UPDATE recipe_stats_info set comment_count_total=comment_count_total+1 ,comment_current_month=comment_current_month+1,"
			+ " last_updated=#{currentTimeMS} where recipe_id=#{recipeId}")
	void updateRecipeStatsTableComment(@Param("recipeId") long recipeId);
	
	
	@Update("UPDATE recipe_stats_info set review_count_total=review_count_total+1 ,review_current_month=review_current_month+1,"
			+ " last_updated=#{currentTimeMS} where recipe_id=#{recipeId}")
	void updateRecipeStatsTableReview(@Param("recipeId") long recipeId);
	
	
	@Update("UPDATE recipe_stats_info set shared_count_total=shared_count_total+1 ,shared_current_month=shared_current_month+1,"
			+ " last_updated=#{currentTimeMS} where recipe_id=#{recipeId}")
	void updateRecipeStatsTableShared(@Param("recipeId") long recipeId);

	
	@Select("select cook_count from recipe_stats_history  where recipe_id = #{recipeId} and month=#{month} and year=#{year} ")
	Integer cookCountForMonth(@Param("recipeId")long recipeId,@Param("month") int month,@Param("year") int year);

	@Update("UPDATE recipe_stats_info set cooked_count_total=cooked_count_total+1 ,cooked_current_month=cooked_current_month+1,"
			+ " collect_previous_month =#{countPreviousMonth} , last_updated=#{currentTimeMS} where recipe_id=#{recipeId}")
	void updateRecipeStatsTableCookPreviousMonthData(@Param("recipeId") long recipeId, @Param("countPreviousMonth") int countPreviousMonth,
			@Param("currentTimeMS") long currentTimeMillis );
}
