package com.reciplay.cis.interceptor;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.reciplay.auth.beans.WebResponse;
import com.reciplay.auth.webclient.AuthClient;
import com.reciplay.cis.common.Constants;

@Provider
public class AuthInterceptor implements ContainerRequestFilter {

	private static final Logger logger = LoggerFactory.getLogger(AuthInterceptor.class);

	@Autowired
	AuthClient authClient;

	public void filter(ContainerRequestContext requestContext) throws IOException {
		
		logger.info("AuthInterceptor filter ");
		
		try {
			WebResponse webRes = authClient.authenticateRequest(requestContext.getHeaderString("Cookie"));

			if (webRes.getStatusCode() != 200 && webRes.getStatusCode() != 201) {

				logger.info("Authentication Failed API ", webRes.getPayload());
				requestContext.abortWith(Response.status(webRes.getStatusCode()).entity(webRes.getPayload()).build());
				return;
			}
			
			logger.info("AuthInterceptor filter "+webRes.getPayload());
			requestContext.setProperty(Constants.WebConstant.AUTHENTICATION_DATA, webRes.getPayload());
			
			
		} catch (Exception e) {
			logger.error("Authentication Exception ", e);
			requestContext.abortWith(Response.status(500).entity("Internal Service Error").build());
			return;
		}
	}

}
