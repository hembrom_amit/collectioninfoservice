package com.reciplay.cis.common;

import java.util.Comparator;

import com.reciplay.cis.beans.RecipeInfo;

public class SortByCurrentMonthCount implements Comparator<RecipeInfo> {

	@Override
	public int compare(RecipeInfo recipe1, RecipeInfo recipe2) {
		return recipe1.getReviewCount()-recipe2.getReviewCount(); 
	}

}
