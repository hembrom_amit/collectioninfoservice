package com.reciplay.cis.loggicaspect;

import javax.ws.rs.core.Response;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * Aspect class for tracking all method / api level data.
 * 
 */
@Configuration
@Aspect
public class LogginAspectAction {

    private static final Logger logger = LoggerFactory.getLogger(LogginAspectAction.class);
  
    /**
     * Does the job of storing the required data point in the Cache.
     * 
     * @param joinPoint
     * @param monitorCalls
     * @return
     * @throws Throwable
     */
    @Around(value = "@annotation(logginAspect)")
    public Object monitorAround(ProceedingJoinPoint joinPoint, LogginAspect logginAspect) throws Throwable {
        String apiName = "";
        Object result;
        try {
            if (logginAspect.methodName().length() > 0) {
                apiName = joinPoint.getSignature().getDeclaringTypeName() + "." + logginAspect.methodName();
            } else {
                apiName = joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName();
            }
        } catch (Exception e) {
            logger.error("MonitoringAspect , Exception in monitorAround before Proceed Action :", e);
        }

        long startTime = System.currentTimeMillis();
        try {
            result = joinPoint.proceed();
        } catch (Exception exception) {
            throw exception;
        }
        long timeTaken = System.currentTimeMillis() - startTime;
        int statusCode=0;
        if (result instanceof Response) {
            Response response = (Response) result;
            if (response.getStatus() > -1) {
            	statusCode =  response.getStatus();
            }
        } 
        logger.info("Method :: "+apiName +" , Input : "+ joinPoint.getArgs() +" Output :: "+result +" Status :: "+statusCode+" , time taken ms :: "+timeTaken);
        return result;
    }
}