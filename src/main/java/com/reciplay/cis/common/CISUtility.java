package com.reciplay.cis.common;

import java.time.LocalDate;
import java.util.List;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.reciplay.cis.beans.UserRecipeStatsInfo;
import com.reciplay.cis.common.Constants.WebConstant;

@Service
public class CISUtility {

	private static final Logger logger = LoggerFactory.getLogger(CISUtility.class);
	
	public UserRecipeStatsInfo convertStringToRecipeStatsInfo(JSONObject jsonObject) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		UserRecipeStatsInfo recipeStatsInfo = null;
		try {
			recipeStatsInfo = mapper.readValue(jsonObject.toString(), UserRecipeStatsInfo.class);
		} catch (Exception ex) {
			throw new ServiceException(Status.BAD_REQUEST, WebConstant.IMPROPER_PAYLOAD,
					WebConstant.IMPROPER_PAYLOAD_ERRCODE, ex);
		}

		logger.info("RecipeStatsInfo POJO " + recipeStatsInfo.toString());
		return recipeStatsInfo;
	}
	
	public String getRecipeString(List<Long> recipeIds) {
		StringBuilder str = new StringBuilder();
		for(int i=0;i<recipeIds.size();i++) {
			if(str.length()>0) {
				str.append(","+recipeIds.get(i));
			} else {
				str.append(recipeIds.get(i)+"");
			}
		}
		return str.toString();
	}

	public String getCurrentMonth(LocalDate currentDate) {
		String currentMonth = currentDate.getMonth().name().toLowerCase();
		return  ( Character.toUpperCase( currentMonth.substring(0, 3).charAt(0) )+  currentMonth.substring(1, 3));
	}

	public String getPreviousMonth(LocalDate currentDate) {
		String previousMonth = currentDate.minusMonths(1).getMonth().name().toLowerCase();
		return  ( Character.toUpperCase( previousMonth.substring(0, 3).charAt(0) )+  previousMonth.substring(1, 3));
	}
	
	
}
