package com.reciplay.cis.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface KeeperDAO {
	
	
	
	@Select("select count(1) from kitchen_recipe kr , kitchen_info ki ,entity_info ei where kr.kitchen_id=ki.id and kr.recipe_id=#{recipeId} and ki.entity_id=ei.id and ei.user_id=#{userId}")
	int validateRecipeInfoForUser(@Param("userId") Long userId,@Param("recipeId") long recipeId);
	
	
	@Select("select count(1) "
			+ "from kitchen_roles kr where kr.user_id=#{userId} and kr.kitchen_id=#{id} and kr.is_deleted=0")
	int getKitchenAdminRoleCount(@Param("userId") long userId,@Param("id")long id);
	
	
}
