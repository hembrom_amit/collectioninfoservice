package com.reciplay.test;

public class DataMongo {

	private String _id;
	private long createdtimestamp;
	private DishInfo dishinfo;
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public long getCreatedtimestamp() {
		return createdtimestamp;
	}
	public void setCreatedtimestamp(long createdtimestamp) {
		this.createdtimestamp = createdtimestamp;
	}
	public DishInfo getDishinfo() {
		return dishinfo;
	}
	public void setDishinfo(DishInfo dishinfo) {
		this.dishinfo = dishinfo;
	}
	@Override
	public String toString() {
		return "DataMongo [_id=" + _id + ", createdtimestamp=" + createdtimestamp + ", dishinfo=" + dishinfo + "]";
	}
	
	
}
