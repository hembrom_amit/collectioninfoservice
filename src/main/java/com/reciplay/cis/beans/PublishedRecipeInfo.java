package com.reciplay.cis.beans;

import java.util.List;

public class PublishedRecipeInfo {

	private CollectionInfo collectionInfo;
	private RecipeStatsVO recipeStats;
	private List<RecipeInfo> recipeList;
	public CollectionInfo getCollectionInfo() {
		return collectionInfo;
	}
	public void setCollectionInfo(CollectionInfo collectionInfo) {
		this.collectionInfo = collectionInfo;
	}
	public RecipeStatsVO getRecipeStats() {
		return recipeStats;
	}
	public void setRecipeStats(RecipeStatsVO recipeStats) {
		this.recipeStats = recipeStats;
	}
	public List<RecipeInfo> getRecipeList() {
		return recipeList;
	}
	public void setRecipeList(List<RecipeInfo> recipeList) {
		this.recipeList = recipeList;
	}
	public PublishedRecipeInfo(CollectionInfo collectionInfo, RecipeStatsVO recipeStats, List<RecipeInfo> recipeList) {
		this.collectionInfo = collectionInfo;
		this.recipeStats = recipeStats;
		this.recipeList = recipeList;
	}
	public PublishedRecipeInfo() {
	}
	@Override
	public String toString() {
		return "PublishedRecipeInfo [collectionInfo=" + collectionInfo + ", recipeStats=" + recipeStats
				+ ", recipeList=" + recipeList + "]";
	}
	
	
	
	
}
