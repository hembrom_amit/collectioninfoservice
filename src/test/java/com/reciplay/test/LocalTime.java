package com.reciplay.test;

import java.time.LocalDate;

public class LocalTime {

	public static void main(String[] args) {

		LocalDate currentDate = LocalDate.now(); // 2016-06-17 
		int month = currentDate.getMonthValue(); // 17 
		int year = currentDate.getYear(); // 169

		String currentMonth = currentDate.getMonth().name().toLowerCase();
		String previousMonth = currentDate.minusMonths(1).getMonth().name().toLowerCase();
		System.out.println("month "+( Character.toUpperCase( previousMonth.substring(0, 3).charAt(0) )+  previousMonth.substring(1, 3)));
		System.out.println("month "+( Character.toUpperCase( currentMonth.substring(0, 3).charAt(0) )+  currentMonth.substring(1, 3)));
	}

}
