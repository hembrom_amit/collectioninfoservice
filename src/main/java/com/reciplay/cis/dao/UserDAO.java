package com.reciplay.cis.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.reciplay.cis.beans.User;

public interface UserDAO {

	@Select("SELECT u.id as userId, u.loginid as loginid, u.firstname as firstname , u.lastname as lastname from users u  where u.id = #{userId}")
	User getUserDataByUserId(@Param("userId") long userId);

}