package com.reciplay.test;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

public class DateFormat {

	public static void main(String[] args) {

		long val = System.currentTimeMillis();
        Date date=new Date(val);
        SimpleDateFormat df2 = new SimpleDateFormat("dd MMM yyyy");
        String dateText = df2.format(date);
        //System.out.println(dateText);
        
        LocalDate currentDate = LocalDate.now(); // 2016-06-17 
		int month = currentDate.getMonthValue(); // 17 
		int year = currentDate.getYear(); // 169

		System.out.println("month "+currentDate.minusMonths(1).getMonth());
		System.out.println("month "+currentDate.getMonth());
	}

}
